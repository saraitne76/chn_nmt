import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))


import time                                         # noqa
from utils import *                                 # noqa
from tf_utils import *                              # noqa
from tensorflow.contrib import rnn, seq2seq         # noqa
from nltk.translate.bleu_score import corpus_bleu   # noqa


class Arguments:
    """
    Network hyper-parameter arguments set
    """

    def __init__(self,
                 cell_type: str,
                 ch_token2id: Dict,
                 kr_token2id: Dict,
                 enc_hiddens: List[int],
                 dec_hiddens: List[int],
                 output_drop: bool,
                 state_drop: bool,
                 ch_emb_dim: int,
                 ch_emb_matrix: Optional[np.ndarray],
                 kr_emb_dim: int,
                 kr_emb_matrix: Optional[np.ndarray],
                 projection_layer: int):
        """
        :param cell_type: RNN cell type
        :param ch_token2id: Chinese Token-to-Index Dictionary
        :param kr_token2id: Korean Token-to-Index Dictionary
        :param enc_hiddens: Encoder RNN Hidden size, Length of list means the number of layers
        :param dec_hiddens: Decoder RNN Hidden size, Length of list means the number of layers
        :param output_drop: Whether for RNN cell to use output drop out or not In train phase
        :param state_drop: Whether for RNN cell to use state drop out or not In train phase
        :param ch_emb_dim: Chinese word embedding dimension
        :param ch_emb_matrix: Chinese word embedding initialization, if None, using xavier initializer
        :param kr_emb_dim: Korean word embedding dimension
        :param kr_emb_matrix: Korean word embedding initialization, if None, using xavier initializer
        :param projection_layer: Decoder Projection Layer: Dense(0), transpose of target embedding matrix(1)
        """
        self.cell_type = cell_type
        self.ch_token2id = ch_token2id
        self.ch_vocab_size = len(self.ch_token2id)
        self.kr_token2id = kr_token2id
        self.kr_vocab_size = len(self.kr_token2id)
        self.enc_hiddens = enc_hiddens
        self.dec_hiddens = dec_hiddens
        self.output_drop = output_drop
        self.state_drop = state_drop
        self.ch_emb_dim = ch_emb_dim
        self.ch_emb_matrix = ch_emb_matrix
        self.kr_emb_dim = kr_emb_dim
        self.kr_emb_matrix = kr_emb_matrix
        self.projection_layer = projection_layer
        return

    def print(self):
        if self.ch_emb_matrix is not None:
            ch_emb_mat_init = 'word2vec'
        else:
            ch_emb_mat_init = 'random'

        if self.kr_emb_matrix is not None:
            kr_emb_mat_init = 'word2vec'
        else:
            kr_emb_mat_init = 'random'

        if self.projection_layer == 0:
            projection_layer = 'Dens Layer'
        elif self.projection_layer == 1:
            projection_layer = 'Transpose of Target Embedding Matrix'
        else:
            raise ValueError('''Decoder Projection Layer: Dense(0), transpose of target embedding matrix(1)
                                But input: %s''' % self.projection_layer)

        s = """
            ===============Model Hyper Parameter===============
            RNN Cell Type: %s

            Chinese Vocabulary Size: %s
                first 5 examples: %s 
                end 5 examples: %s
                random 5 examples: %s

            Korean Vocabulary Size: %s
                first 5 examples: %s 
                end 5 examples: %s
                random 5 examples: %s

            Encoder Hidden: %s
            Decoder Hidden: %s
            RNN Output Drop-out: %s
            RNN State Drop-out: %s
            Chinese Embedding Size: %s
            Chinese Embedding Init: %s
            Korean Embedding Size: %s
            Korean Embedding Init: %s
            Decoder Output Projection: %s
            ===================================================
            \n\r""" % (self.cell_type,
                       self.ch_vocab_size,
                       list(self.ch_token2id.items())[:5],
                       list(self.ch_token2id.items())[-5:],
                       random.sample(list(self.ch_token2id.items()), 5),
                       self.kr_vocab_size,
                       list(self.kr_token2id.items())[:5],
                       list(self.kr_token2id.items())[-5:],
                       random.sample(list(self.kr_token2id.items()), 5),
                       self.enc_hiddens,
                       self.dec_hiddens,
                       self.output_drop,
                       self.state_drop,
                       self.ch_emb_dim,
                       ch_emb_mat_init,
                       self.kr_emb_dim,
                       kr_emb_mat_init,
                       projection_layer)
        return s


class Seq2SeqAttention:
    """
    Model Description
    Chinese Character token Embedding
    Korean Morpheme(형태소) token Embedding
    Sequence-to-Sequence with BahdanauAttention Model
    """
    def __init__(self,
                 name: str,
                 args: Optional[Arguments] = None,
                 config_rewrite=False):
        self.log_dir = os.path.join(LOG_DIR, name)
        self.param_dir = os.path.join(PARAMS_DIR, name)
        os.makedirs(self.log_dir, exist_ok=True)
        os.makedirs(self.param_dir, exist_ok=True)

        if args is None:
            args = pickle_load(os.path.join(self.log_dir, CONFIG_PICKLE))

        if not os.path.exists(os.path.join(self.log_dir, CONFIG_PICKLE)) or config_rewrite:
            pickle_store(args, os.path.join(self.log_dir, CONFIG_PICKLE))
            with open(os.path.join(self.log_dir, CONFIG_TXT), 'w') as f:
                print_write(self.__doc__, f)
                print_write(args.__init__.__doc__, f)
                print_write(args.print(), f)

        self.hyper_params = args

        self.ch_token2id = args.ch_token2id
        self.ch_id2token = dict_swap(args.ch_token2id)

        self.kr_token2id = args.kr_token2id
        self.kr_id2token = dict_swap(args.kr_token2id)

        self.global_step = tf.get_variable('GlobalStep', [],
                                           dtype=tf.int32,
                                           initializer=tf.initializers.constant(0),
                                           trainable=False)

        self.lr = tf.get_variable('LearningRate', [],
                                  dtype=tf.float32,
                                  initializer=tf.initializers.zeros(),
                                  trainable=False)
        self.lr_placeholder = tf.placeholder(tf.float32)
        self.lr_update = tf.assign(self.lr, self.lr_placeholder)

        self.kp = tf.get_variable('KeepProb', [],
                                  dtype=tf.float32,
                                  initializer=tf.initializers.ones(),
                                  trainable=False)
        self.kp_placeholder = tf.placeholder(tf.float32)
        self.kp_update = tf.assign(self.kp, self.kp_placeholder)
        return

    def _placeholders(self):
        self.enc_inp = tf.placeholder(tf.int32, [self.batch_size, self.enc_max_len], name='EncoderInput')
        self.enc_seq_len = tf.placeholder(tf.int32, [self.batch_size], name='EncSeqLen')

        self.dec_inp = tf.placeholder(tf.int32, [self.batch_size, self.dec_max_len], name='DecoderInput')
        self.dec_target = tf.placeholder(tf.int32, [self.batch_size, self.dec_max_len], name='DecoderTarget')
        self.dec_seq_len = tf.placeholder(tf.int32, [self.batch_size], name='DecSeqLen')
        return

    def build_model(self,
                    train_phase: bool,
                    num_gpu: int,
                    batch_size: Optional[int],
                    enc_max_len: Optional[int],
                    dec_max_len: Optional[int],
                    gradients_clip: Optional[float],
                    beam_width: Optional[int],
                    alignment_history: Optional[bool]):
        self._build_model(train_phase=train_phase,
                          num_gpu=num_gpu,
                          batch_size=batch_size,
                          enc_max_len=enc_max_len,
                          dec_max_len=dec_max_len,
                          gradients_clip=gradients_clip,
                          beam_width=beam_width,
                          alignment_history=alignment_history)
        return

    def _build_model(self,
                     train_phase: bool,
                     num_gpu: int,
                     batch_size: Optional[int],
                     enc_max_len: Optional[int],
                     dec_max_len: Optional[int],
                     gradients_clip: Optional[float],
                     beam_width: Optional[int],
                     alignment_history: Optional[bool]):
        self.train_phase = train_phase
        self.num_gpu = num_gpu
        self.batch_size = batch_size
        self.enc_max_len = enc_max_len
        self.dec_max_len = dec_max_len
        self.gradients_clip = gradients_clip

        self.beam_width = beam_width
        self.alignment_history = alignment_history

        self._placeholders()

        if self.train_phase:
            optimizer = tf.train.AdamOptimizer(self.lr)
            if self.num_gpu == 1:
                logit, self.states = self._net(enc_inp=self.enc_inp,
                                               enc_seq_len=self.enc_seq_len,
                                               dec_inp=self.dec_inp,
                                               dec_seq_len=self.dec_seq_len,
                                               batch_size=self.batch_size)
                loss = self.get_loss(logit=logit,
                                     target=self.dec_target,
                                     target_seq_len=self.dec_seq_len,
                                     target_max_len=self.dec_max_len)
                self._loss, self._loss_op = tf.metrics.mean(loss, name='LossMean')
                grads_and_vars = optimizer.compute_gradients(loss)
                if self.gradients_clip is not None:
                    grads_and_vars = clip_gradients(grads_and_vars, self.gradients_clip)
                self.train_op = optimizer.apply_gradients(grads_and_vars, global_step=self.global_step)
            else:
                if (self.batch_size % self.num_gpu) != 0:
                    raise ValueError('batch_size: %d, num_gpu: %d' % (self.batch_size, self.num_gpu))

                tower_batch = self.batch_size // self.num_gpu

                sp_enc_inp = tf.split(self.enc_inp, self.num_gpu)
                sp_enc_seq_len = tf.split(self.enc_seq_len, self.num_gpu)

                sp_dec_inp = tf.split(self.dec_inp, self.num_gpu)
                sp_dec_target = tf.split(self.dec_target, self.num_gpu)
                sp_dec_seq_len = tf.split(self.dec_seq_len, self.num_gpu)

                tower_loss = list()
                tower_gradients = list()

                for i in range(self.num_gpu):
                    with tf.device('/gpu:%d' % i):
                        with tf.name_scope('tower_%d' % i):
                            with tf.variable_scope(tf.get_variable_scope(), reuse=tf.AUTO_REUSE):
                                logit, self.states = self._net(enc_inp=sp_enc_inp[i],
                                                               enc_seq_len=sp_enc_seq_len[i],
                                                               dec_inp=sp_dec_inp[i],
                                                               dec_seq_len=sp_dec_seq_len[i],
                                                               batch_size=tower_batch)
                                loss = self.get_loss(logit=logit,
                                                     target=sp_dec_target[i],
                                                     target_seq_len=sp_dec_seq_len[i],
                                                     target_max_len=self.dec_max_len)
                                tower_loss.append(loss)
                                grads_and_vars = optimizer.compute_gradients(loss)
                                if self.gradients_clip is not None:
                                    grads_and_vars = clip_gradients(grads_and_vars, self.gradients_clip)
                                tower_gradients.append(grads_and_vars)

                total_loss = tf.reduce_mean(tower_loss)
                self._loss, self._loss_op = tf.metrics.mean(total_loss, name='LossMean')
                total_grad = merge_gradients(tower_gradients)
                self.train_op = optimizer.apply_gradients(total_grad, global_step=self.global_step)

            train_summaries = list()
            with tf.name_scope('General'):
                train_summaries.append(tf.summary.scalar('Loss', self._loss))
                train_summaries.append(tf.summary.scalar('LearningRate', self.lr))
            self.train_merged = tf.summary.merge(train_summaries)
        else:
            self.prediction, self.states = self._net(enc_inp=self.enc_inp,
                                                     enc_seq_len=self.enc_seq_len,
                                                     dec_inp=self.dec_inp,
                                                     dec_seq_len=self.dec_seq_len,
                                                     batch_size=self.batch_size)
        self.saver = tf.train.Saver()
        self.get_trainable_var = tf.trainable_variables()
        self.global_var_init = tf.global_variables_initializer()
        self.local_var_init = tf.local_variables_initializer()
        return

    def _encoder_embeddings(self, enc_inp, scope: str):
        with tf.variable_scope(scope):
            enc_emb_inp, self.ch_emb_matrix = self.ch_token_emb(enc_inp, name='ChineseCharEmb')
        return enc_emb_inp

    def _decoder_embeddings(self, dec_inp, scope: str):
        with tf.variable_scope(scope):
            dec_emb_inp, self.kr_emb_matrix = self.kr_token_emb(dec_inp, name='KoreanMorpEmb')
        return dec_emb_inp

    def ch_token_emb(self,
                     idx_inp: tf.Tensor,
                     name: str):
        with tf.variable_scope(name):
            if self.hyper_params.ch_emb_matrix is not None:
                embed_init = tf.constant_initializer(self.hyper_params.ch_emb_matrix, verify_shape=True)
            else:
                embed_init = xavier_initializer()
            emb_matrix = tf.get_variable('EmbeddingMatrix',
                                         shape=[self.hyper_params.ch_vocab_size, self.hyper_params.ch_emb_dim],
                                         dtype=tf.float32,
                                         initializer=embed_init,
                                         trainable=True)
            embedding = tf.nn.embedding_lookup(emb_matrix, idx_inp)
        return embedding, emb_matrix

    def kr_token_emb(self,
                     idx_inp: tf.Tensor,
                     name: str):
        with tf.variable_scope(name):
            if self.hyper_params.kr_emb_matrix is not None:
                embed_init = tf.constant_initializer(self.hyper_params.kr_emb_matrix, verify_shape=True)
            else:
                embed_init = xavier_initializer()
            emb_matrix = tf.get_variable('EmbeddingMatrix',
                                         shape=[self.hyper_params.kr_vocab_size, self.hyper_params.kr_emb_dim],
                                         dtype=tf.float32,
                                         initializer=embed_init,
                                         trainable=True)
            embedding = tf.nn.embedding_lookup(emb_matrix, idx_inp)
        return embedding, emb_matrix

    def _net(self,
             enc_inp,
             enc_seq_len: tf.Tensor,
             dec_inp: tf.Tensor,
             dec_seq_len: tf.Tensor,
             batch_size: int):

        if self.train_phase:
            output_drop = self.hyper_params.output_drop
            state_drop = self.hyper_params.state_drop
        else:
            output_drop = False
            state_drop = False

        enc_emb_inp = self._encoder_embeddings(enc_inp, scope='EncoderEmbeddings')
        dec_emb_inp = self._decoder_embeddings(dec_inp, scope='DecoderEmbeddings')
        self.enc_inp_info = enc_emb_inp.__str__()
        self.dec_inp_info = dec_emb_inp.__str__()

        encoder_outputs, encoder_states = self.encoder(enc_emb_inp=enc_emb_inp,
                                                       seq_len=enc_seq_len,
                                                       keep_prob=self.kp,
                                                       output_drop=output_drop,
                                                       state_drop=state_drop,
                                                       name='Encoder')
        decoder_outputs, decoder_states = self.decoder(encoder_outputs=encoder_outputs,
                                                       encoder_states=encoder_states,
                                                       enc_seq_len=enc_seq_len,
                                                       target_inp=dec_emb_inp,
                                                       target_seq_len=dec_seq_len,
                                                       target_emb_matrix=self.kr_emb_matrix,
                                                       keep_prob=self.kp,
                                                       output_drop=output_drop,
                                                       state_drop=state_drop,
                                                       batch_size=batch_size,
                                                       name='Decoder')
        if self.train_phase:
            logits = decoder_outputs
            return logits, decoder_states
        else:
            prediction = decoder_outputs
            return prediction, decoder_states

    def encoder(self,
                enc_emb_inp: tf.Tensor,
                seq_len: tf.Tensor,
                keep_prob,
                output_drop,
                state_drop,
                name='Encoder'):
        with tf.variable_scope(name):
            fw_cells = rnn_cells(cell_type=self.hyper_params.cell_type,
                                 hidden_list=self.hyper_params.enc_hiddens,
                                 keep_prob=keep_prob,
                                 output_drop=output_drop,
                                 state_drop=state_drop)
            bw_cells = rnn_cells(cell_type=self.hyper_params.cell_type,
                                 hidden_list=self.hyper_params.enc_hiddens,
                                 keep_prob=keep_prob,
                                 output_drop=output_drop,
                                 state_drop=state_drop)

            encoder_outputs, state_fw, state_bw = \
                rnn.stack_bidirectional_dynamic_rnn(cells_fw=fw_cells,
                                                    cells_bw=bw_cells,
                                                    inputs=enc_emb_inp,
                                                    sequence_length=seq_len,
                                                    dtype=tf.float32)

            state_c = tf.concat((state_fw[-1].c, state_bw[-1].c), 1)
            state_h = tf.concat((state_fw[-1].h, state_bw[-1].h), 1)
            encoder_states = rnn.LSTMStateTuple(c=state_c, h=state_h)
        return encoder_outputs, encoder_states

    def decoder(self,
                encoder_outputs: tf.Tensor,
                encoder_states: rnn.LSTMStateTuple,
                enc_seq_len: tf.Tensor,
                target_inp: tf.Tensor,
                target_seq_len: tf.Tensor,
                target_emb_matrix: tf.Variable,
                keep_prob,
                output_drop,
                state_drop,
                batch_size,
                name='Decoder'):
        with tf.variable_scope(name) as scope:
            decoder_cells = rnn_cells(self.hyper_params.cell_type,
                                      self.hyper_params.dec_hiddens,
                                      keep_prob,
                                      output_drop,
                                      state_drop)

            if self.hyper_params.projection_layer == 0:
                dec_oup_proj = tf.layers.Dense(self.hyper_params.kr_vocab_size, use_bias=False, name='DecOupProj')
            elif self.hyper_params.projection_layer == 1:
                dec_oup_proj = OutputLayer(num_inputs=self.hyper_params.dec_hiddens[-1],
                                           embedding_matrix=target_emb_matrix)
            else:
                raise ValueError('''Decoder Projection Layer: Dense(0), transpose of target embedding matrix(1)
                                    But input: %s''' % self.hyper_params.projection_layer)

            if self.train_phase:
                attention_mechanism = seq2seq.BahdanauAttention(num_units=self.hyper_params.dec_hiddens[0],
                                                                memory=encoder_outputs,
                                                                memory_sequence_length=enc_seq_len,
                                                                normalize=True)
                decoder_cells[0] = seq2seq.AttentionWrapper(decoder_cells[0], attention_mechanism)
                first_cell_state = decoder_cells[0].zero_state(batch_size=batch_size, dtype=tf.float32)
                first_cell_state = [first_cell_state.clone(cell_state=encoder_states)]
                remain_cell_state = [cell.zero_state(batch_size, dtype=tf.float32) for cell in decoder_cells[1:]]

                initial_state = tuple(first_cell_state + remain_cell_state)

                helper = seq2seq.TrainingHelper(inputs=target_inp,
                                                sequence_length=target_seq_len,
                                                time_major=False)
                if len(decoder_cells) > 1:
                    decoder_cell = tf.nn.rnn_cell.MultiRNNCell(decoder_cells)
                else:
                    decoder_cell = decoder_cells[0]
                    initial_state = initial_state[0]

                decoder = seq2seq.BasicDecoder(decoder_cell, helper, initial_state)
                outputs, states, _ = seq2seq.dynamic_decode(decoder, output_time_major=False, scope=scope)
                logits = dec_oup_proj(outputs.rnn_output)
                pad = tf.zeros([batch_size,
                                self.dec_max_len-tf.shape(logits)[1],
                                self.hyper_params.kr_vocab_size])
                logits = tf.concat([logits, pad], axis=1)
                # print(logits)
                return logits, states
            else:
                tiled_encoder_output = seq2seq.tile_batch(encoder_outputs, multiplier=self.beam_width)
                tiled_encoder_state = seq2seq.tile_batch(encoder_states, multiplier=self.beam_width)
                tiled_seq_len = seq2seq.tile_batch(enc_seq_len, multiplier=self.beam_width)

                attention_mechanism = seq2seq.BahdanauAttention(num_units=self.hyper_params.dec_hiddens[0],
                                                                memory=tiled_encoder_output,
                                                                memory_sequence_length=tiled_seq_len,
                                                                normalize=True)
                # attention mechanism applied on first decoder cell
                decoder_cells[0] = seq2seq.AttentionWrapper(decoder_cells[0], attention_mechanism,
                                                            alignment_history=self.alignment_history)
                first_cell_state = decoder_cells[0].zero_state(batch_size=batch_size*self.beam_width,
                                                               dtype=tf.float32)
                first_cell_state = [first_cell_state.clone(cell_state=tiled_encoder_state)]
                remain_cell_state = [cell.zero_state(batch_size*self.beam_width, dtype=tf.float32)
                                     for cell in decoder_cells[1:]]

                initial_state = tuple(first_cell_state + remain_cell_state)

                if len(decoder_cells) > 1:
                    decoder_cell = tf.nn.rnn_cell.MultiRNNCell(decoder_cells)
                else:
                    decoder_cell = decoder_cells[0]
                    initial_state = initial_state[0]

                if self.beam_width > 1:
                    decoder = seq2seq.BeamSearchDecoder(
                        cell=decoder_cell,
                        embedding=target_emb_matrix,
                        start_tokens=tf.fill([batch_size], tf.constant(self.kr_token2id[GO])),
                        end_token=tf.constant(self.kr_token2id[EOS]),
                        initial_state=initial_state,
                        beam_width=self.beam_width,
                        output_layer=dec_oup_proj
                    )
                    outputs, states, _ = seq2seq.dynamic_decode(decoder,
                                                                maximum_iterations=self.dec_max_len,
                                                                output_time_major=False,
                                                                scope=scope)
                    prediction = outputs.predicted_ids
                    # class BeamSearchDecoderState(
                    #     collections.namedtuple("BeamSearchDecoderState",
                    #                            ("cell_state", "log_probs", "finished", "lengths"))):

                    if len(decoder_cells) > 1:
                        # return prediction, states[0]      # Greedy Search
                        return prediction, states.cell_state[0]
                    else:
                        return prediction, states.cell_state
                else:
                    helper = seq2seq.GreedyEmbeddingHelper(
                        embedding=target_emb_matrix,
                        start_tokens=tf.fill([batch_size], tf.constant(self.kr_token2id[GO])),
                        end_token=tf.constant(self.kr_token2id[EOS])
                    )
                    decoder = seq2seq.BasicDecoder(
                        cell=decoder_cell,
                        helper=helper,
                        initial_state=initial_state,
                        output_layer=dec_oup_proj
                    )
                    outputs, states, _ = seq2seq.dynamic_decode(decoder,
                                                                maximum_iterations=self.dec_max_len,
                                                                output_time_major=False,
                                                                scope=scope)
                    prediction = outputs.sample_id
                    if len(decoder_cells) > 1:
                        return prediction, states[0]
                    else:
                        return prediction, states

    @staticmethod
    def get_loss(logit, target, target_seq_len, target_max_len: int):
        weights = tf.sequence_mask(target_seq_len, target_max_len, dtype=tf.float32)
        loss = seq2seq.sequence_loss(logits=logit, targets=target, weights=weights)
        return loss

    def build_dataset(self, data_list: List[PairData]):
        dataset = DataSet(enc_max_len=self.enc_max_len,
                          dec_max_len=self.dec_max_len,
                          ch_word2id=self.ch_token2id,
                          kr_word2id=self.kr_token2id)
        dataset.make(pairs=data_list,
                     train_phase=self.train_phase)
        return dataset

    def init_model(self, sess, log_file=None, ckpt=None, variables_rewrite=False):
        if ckpt:
            self.saver.restore(sess, os.path.join(self.param_dir, ckpt, PARAMS))
            if log_file:
                print_write('model loaded from file: %s\n' % os.path.join(self.log_dir, ckpt),
                            os.path.join(self.log_dir, log_file), 'a')
        else:
            sess.run(self.global_var_init)
            if log_file:
                with open(os.path.join(self.log_dir, log_file), 'w') as f:
                    print_write('global variable initialize\n', f)

            writer = tf.summary.FileWriter(os.path.join(self.log_dir, 'train'), filename_suffix='-graph')
            writer.add_graph(sess.graph)
            writer.close()

        if not ckpt or variables_rewrite:
            with open(os.path.join(self.log_dir, CONFIG_TXT), 'a') as f:
                # print_write('input embed: %s\n' % self.enc_emb_inp, f)
                print_write('===============================================================================\n', f)
                print_write('Encoder Input: %s\n' % self.enc_inp_info, f)
                print_write('Decoder Input: %s\n' % self.dec_inp_info, f)
                print_write('===============================================================================\n', f)
                count_vars = 0
                for var in self.get_trainable_var:
                    name = var.name
                    shape = var.shape.as_list()
                    num_elements = var.shape.num_elements()
                    print_write('Variable name: %s\n' % name, f)
                    print_write('Placed device: %s\n' % var.device, f)
                    print_write('Shape : %s  Elements: %d\n' % (str(shape), num_elements), f)
                    print_write('===============================================================================\n', f)
                    count_vars = count_vars + num_elements
                print_write('Total number of trainable variables %d\n' % count_vars, f)
                print_write('===============================================================================\n', f)
        return

    def get_train_feed(self, dataset):
        enc_inp, enc_len, dec_inp, dec_trg, dec_len = dataset.get_train_batch(self.batch_size)
        feed = {self.enc_inp: enc_inp,
                self.enc_seq_len: enc_len,
                self.dec_inp: dec_inp,
                self.dec_target: dec_trg,
                self.dec_seq_len: dec_len}
        return feed

    def train(self,
              sess: tf.Session,
              data_list: List[PairData],
              train_step: int,
              lr: float,
              keep_prob=0.7,
              ckpt=None,
              summary_step=1000,
              variable_rewrite=False):
        assert (train_step % summary_step) == 0, 'train_step: %d, summary_step: %d' % (train_step, summary_step)

        dataset = self.build_dataset(data_list)

        self.init_model(sess, TRAIN_LOG, ckpt, variable_rewrite)
        global_step = sess.run(self.global_step)
        start_step = global_step
        sess.run([self.local_var_init, self.lr_update, self.kp_update],
                 feed_dict={self.lr_placeholder: lr, self.kp_placeholder: keep_prob})
        with open(os.path.join(self.log_dir, TRAIN_LOG), 'a') as f:
            lr_v, kp_v = sess.run([self.lr, self.kp])
            print_write(self.get_running_configure(dataset, start_step, train_step, lr_v, kp_v), f)

        s = time.time()
        for i in range(train_step // (summary_step * 10)):
            writer = tf.summary.FileWriter(os.path.join(self.log_dir, 'train'),
                                           filename_suffix='-step-%d' % global_step)
            for j in range(summary_step * 10):
                feed = self.get_train_feed(dataset)
                _, loss = sess.run([self.train_op, self._loss_op], feed_dict=feed)
                global_step = sess.run(self.global_step)

                print('\rTraining - Loss: %0.3f, step %d/%d'
                      % (loss, global_step, train_step + start_step), end='')

                if global_step % summary_step == 0:
                    merged, loss = sess.run([self.train_merged, self._loss])
                    writer.add_summary(merged, global_step)

                    print('\r', end='')
                    print_write('Training - Loss: %0.3f, step: %d, %0.2f sec/step\n'
                                % (loss, global_step, (time.time() - s) / summary_step),
                                os.path.join(self.log_dir, TRAIN_LOG), 'a')

                    s = time.time()
                    sess.run(self.local_var_init)

            print_write('global step: %d, model save, time: %s\n'
                        % (global_step, time.strftime('%y-%m-%d %H:%M:%S')),
                        os.path.join(self.log_dir, TRAIN_LOG), 'a')
            writer.close()
            self.saver.save(sess, os.path.join(self.param_dir, 'step-%d' % global_step, PARAMS))

            s = time.time()
        return

    def eval_init(self,
                  sess: tf.Session,
                  data_list: List[PairData],
                  ckpt='step-',
                  test_log=None,
                  variable_rewrite=False):
        if test_log is None:
            test_log = ckpt + '.txt'

        dataset = self.build_dataset(data_list)
        self.init_model(sess, log_file=None, ckpt=ckpt, variables_rewrite=variable_rewrite)
        global_step = sess.run(self.global_step)

        with open(os.path.join(self.log_dir, test_log), 'w') as f:
            print_write(self.get_running_configure(dataset, global_step, None, None, None), f)

        sess.run(self.local_var_init)
        return dataset, test_log

    def get_eval_feed(self, dataset):
        # enc_seq, enc_inp, enc_len, dec_seq, dec_inp, dec_trg, dec_len = dataset.get_eval_batch(self.batch_size)
        enc_tokens, enc_inp, enc_len, dec_tokens = dataset.get_eval_batch(self.batch_size)
        n_real_data = enc_inp.shape[0]
        end = False
        if dataset.num_data == dataset.sequential_indices:
            end = True
        elif dataset.num_data < dataset.sequential_indices:
            end = True
            fill_size = self.batch_size - n_real_data
            enc_inp = np.concatenate([enc_inp, np.zeros((fill_size, self.enc_max_len), dtype=np.uint16)], axis=0)
            enc_len = np.concatenate([enc_len, np.ones(fill_size, dtype=np.uint16)], axis=0)
        else:
            pass
        feed = {self.enc_inp: enc_inp,
                self.enc_seq_len: enc_len}
        return feed, enc_tokens, dec_tokens, n_real_data, end

    def eval(self,
             sess: tf.Session,
             data_list: List[PairData],
             ckpt='step-',
             test_log=None,
             num_examples=5,
             seed=777,
             variable_rewrite=False):
        dataset, test_log = self.eval_init(sess, data_list, ckpt, test_log, variable_rewrite)

        src = []
        ref = []
        hyp = []
        dataset.sequential_indices = 0
        cnt = 0
        end = False
        while not end:
            feed, source, target, n_real_data, end = self.get_eval_feed(dataset)
            cnt += n_real_data
            prediction = sess.run(self.prediction, feed_dict=feed)
            # [batch, seq_length, beam_width]
            if self.beam_width > 1:
                prediction = prediction[:, :, 0]

            for i in range(n_real_data):
                src.append(source[i])
                ref.append([target[i]])
                hyp.append(ids2seq(prediction[i, :], self.kr_id2token, EOS))

            print('\rEvaluation... %d/%d' % (cnt, dataset.num_data), end='')
        print()
        self.print_eval_result(src, ref, hyp, test_log, num_examples, seed)
        return

    def eval_with_ner(self,
                      sess_nmt: tf.Session,
                      sess_ner: tf.Session,
                      ner_model,
                      entity_dic: Dict,
                      data_list: List[PairData],
                      ckpt='step-',
                      test_log=None,
                      num_examples=5,
                      seed=777,
                      variable_rewrite=False):
        dataset, test_log = self.eval_init(sess_nmt, data_list, ckpt, test_log, variable_rewrite)

        src = []
        ref = []
        hyp = []
        hyp_m = []
        nes = []
        dataset.sequential_indices = 0
        cnt = 0
        end = False
        while not end:
            feed, source, target, n_real_data, end = self.get_eval_feed(dataset)
            cnt += n_real_data

            prediction, att_maps = sess_nmt.run([self.prediction, self.states.alignment_history.stack()],
                                                feed_dict=feed)
            if self.beam_width > 1:
                prediction = prediction[:, :, 0]         # [batch, output_len, beam] take first beam

            # [output_len, batch*beam, input_len] -> [batch][output_len, beam, input_len]
            # If Greedy, [batch][output_len, 1, input_len]
            att_maps = np.split(att_maps, self.batch_size, axis=1)

            src_temp = []
            hyp_temp = []
            for i in range(n_real_data):
                src_temp.append(source[i])
                hyp_temp.append(ids2seq(prediction[i, :], self.kr_id2token, EOS))

            entities = ner_model.runs(sess_ner, src_temp, n_real_data)
            for i in range(n_real_data):
                if len(hyp_temp[i]) > 0:
                    # get first beam attention map
                    att_map = att_maps[i][:len(hyp_temp[i]), 0, :len(src_temp[i])]
                    ne_temp = entity_lookup(att_map, entities[i], hyp_temp[i])
                    hyp_m.append(entity_replace(hyp_temp[i], ne_temp, entity_dic))
                    ref.append([target[i]])
                    nes.append(ne_temp)
                else:
                    hyp_m.append(hyp_temp[i])
                    ref.append([target[i]])
                    nes.append([])

            src.extend(src_temp)
            hyp.extend(hyp_temp)

            print('\rEvaluation... %d/%d' % (cnt, dataset.num_data), end='')
        print()

        self.print_eval_result_with_ner(src, ref, hyp, hyp_m, nes, test_log, num_examples, seed)
        return

    def eval_with_dic_ner(self,
                          sess_nmt: tf.Session,
                          gazetteer,
                          entity_dic: Dict,
                          data_list: List[PairData],
                          ckpt='step-',
                          test_log=None,
                          num_examples=5,
                          seed=777,
                          variable_rewrite=False):
        dataset, test_log = self.eval_init(sess_nmt, data_list, ckpt, test_log, variable_rewrite)

        src = []
        ref = []
        hyp = []
        hyp_m = []
        nes = []
        dataset.sequential_indices = 0
        cnt = 0
        end = False
        while not end:
            feed, source, target, n_real_data, end = self.get_eval_feed(dataset)
            cnt += n_real_data

            prediction, att_maps = sess_nmt.run([self.prediction, self.states.alignment_history.stack()],
                                                feed_dict=feed)
            # [batch, seq_length, beam_width]
            prediction = prediction[:, :, 0]
            # [dec_len, tiled_batch, enc_len]
            att_maps = np.split(att_maps, self.batch_size, axis=1)

            src_temp = []
            hyp_temp = []
            entities = []
            for i in range(n_real_data):
                src_temp.append(source[i])
                hyp_temp.append(ids2seq(prediction[i, :], self.kr_id2token, EOS))
                entities.append(search_entity_by_dictionary(''.join(source[i]), gazetteer))

            for i in range(n_real_data):
                # get first beam attention map
                if len(hyp_temp[i]) > 0:
                    att_map = att_maps[i][:len(hyp_temp[i]), 0, :len(src_temp[i])]
                    ne_temp = entity_lookup(att_map, entities[i], hyp_temp[i])
                    hyp_m.append(entity_replace(hyp_temp[i], ne_temp, entity_dic))
                    ref.append([target[i]])
                    nes.append(ne_temp)
                else:
                    hyp_m.append(hyp_temp[i])
                    ref.append([target[i]])
                    nes.append([])

            src.extend(src_temp)
            hyp.extend(hyp_temp)

            print('\rEvaluation... %d/%d' % (cnt, dataset.num_data), end='')
        print()

        self.print_eval_result_with_ner(src, ref, hyp, hyp_m, nes, test_log, num_examples, seed)
        return

    def print_eval_result(self, source, references, hypotheses, log, num_examples, seed):
        t = 'len(src) = %d, len(ref) = %d, len(hyp) = %d' % (len(source), len(references), len(hypotheses))
        assert len(source) == len(references) == len(hypotheses), t

        num_results = len(hypotheses)

        s = time.time()
        bleu = corpus_bleu(references, hypotheses)
        bleu_calc_time = time.time() - s
        with open(os.path.join(self.log_dir, log), 'a') as f:
            s = """
            End Time: %s
            Bleu-4 Score: %0.4f
            Bleu-4 calculation time: %0.2f
            The number of predictions: %d
            Examples
            """ % (time.strftime('%y-%m-%d %H:%M:%S'), bleu, bleu_calc_time, num_results)
            print_write(s, f)
            random.seed(seed)        # Always print same examples of result
            for i in range(num_examples):
                n = random.randint(0, num_results-1)
                s = """
                    source[%d]: %s
                 reference[%d]: %s
                hypothesis[%d]: %s
                """ % (n, ' '.join(source[n]), n, ' '.join(references[n][0]), n, ' '.join(hypotheses[n]))
                print_write(s, f)
        return

    def print_eval_result_with_ner(self,
                                   source,
                                   references,
                                   hypotheses,
                                   hypotheses_modified,
                                   entities,
                                   log,
                                   num_examples,
                                   seed):
        t = 'len(src) = %d, len(ref) = %d, len(hyp) = %d' % (len(source), len(references), len(hypotheses))
        assert len(source) == len(references) == len(hypotheses), t
        # print(hypotheses_modified)
        num_results = len(hypotheses)

        s = time.time()
        bleu = corpus_bleu(references, hypotheses)
        bleu_m = corpus_bleu(references, hypotheses_modified)
        bleu_calc_time = time.time() - s
        with open(os.path.join(self.log_dir, log), 'a') as f:
            s = """
            End Time: %s
              Normal Bleu-4 Score: %0.4f
            Modified Bleu-4 Score: %0.4f
            Bleu-4 calculation time: %0.2f
            The number of predictions: %d
            Examples
            """ % (time.strftime('%y-%m-%d %H:%M:%S'), bleu, bleu_m, bleu_calc_time, num_results)
            print_write(s, f)
            random.seed(seed)        # Always print same examples of result
            for i in range(num_examples):
                n = random.randint(0, num_results-1)
                s = """
                    source[%d]: %s
                 reference[%d]: %s
                hypothesis[%d]: %s
                   hyp_mod[%d]: %s
                  entities[%d]: %s
                """ % (n, ' '.join(source[n]),
                       n, ' '.join(references[n][0]),
                       n, ' '.join(hypotheses[n]),
                       n, ' '.join(hypotheses_modified[n]),
                       n, entities[n])
                print_write(s, f)
        return

    def run(self, sess, sentence: str, cleaning: bool):
        if cleaning:
            s = clean_up_ch(sentence)
        else:
            s = sentence

        char_seq = list(s)
        seq_len = len(char_seq)
        if seq_len < 1:
            raise ValueError('seq_len < 1,   seq_len: %d' % seq_len)
        else:
            ch_ids = seq2ids(char_seq, self.ch_token2id)

            feed = {self.enc_inp: [ch_ids],
                    self.enc_seq_len: [seq_len]}

            prediction, att_wgt = sess.run([self.prediction, self.states.alignment_history.stack()], feed_dict=feed)

            # print(att_wgt.shape) -> [dec_len, tiled_batch, enc_len]
            # tiled_batch = beam_width * batch_size
            # [batch, seq_length, beam_width]
            prediction = prediction[0, :, 0]
            kr_seq = ids2seq(prediction, self.kr_id2token, EOS)

        return kr_seq, att_wgt[:len(kr_seq), 0, :seq_len]

    def get_running_configure(self,
                              dataset,
                              start_step: Optional[int],
                              train_step: Optional[int],
                              lr: Optional[float],
                              keep_prob: Optional[float]):
        enc_inp, enc_len, dec_inp, dec_trg, dec_len = dataset.get_train_batch(batch_size=3)
        if self.train_phase:
            out_string = self.get_training_configure(dataset, start_step, train_step, lr, keep_prob,
                                                     enc_inp, enc_len, dec_inp, dec_trg, dec_len)
        else:
            out_string = self.get_evaluation_configure(dataset, start_step,
                                                       enc_inp, enc_len, dec_inp, dec_trg, dec_len)
        return out_string

    def get_training_configure(self, dataset, start_step, train_step, lr, keep_prob,
                               enc_inp, enc_len, dec_inp, dec_trg, dec_len):
        out_string = """
        ==============================Training Configure==============================
        Training Step: %s ~ %s
        The Number of GPU: %s
        Learning Rate: %s
        Drop-Out Keep Probability: %s 
        Start Time: %s
        DataSet Description
        %s
        The Number of Data: %s
        Encoder Sequence Max Length: %s
        Decoder Sequence Max Length: %s
        Batch Size: %s
        Gradients Clipping by Global Norm: %s

        Encoder Input Data
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################

        Encoder Sequence Length
        %s

        Decoder Input Data
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################

        Decoder Target Data
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################

        Decoder Sequence Length
        %s
        ==============================================================================
        \n\r""" % (start_step,
                   start_step + train_step,
                   self.num_gpu,
                   lr,
                   keep_prob,
                   time.strftime('%y-%m-%d %H:%M:%S'),
                   dataset.__doc__,
                   dataset.num_data,
                   self.enc_max_len,
                   self.dec_max_len,
                   self.batch_size,
                   self.gradients_clip,
                   # ================================================
                   # ================================================
                   ' '.join(map(lambda x: str(x), enc_inp[0, enc_inp[0, :] != self.ch_token2id[PAD]])),
                   ' '.join(ids2seq(enc_inp[0, :], self.ch_id2token, PAD)),
                   ' '.join(map(lambda x: str(x), enc_inp[1, enc_inp[1, :] != self.ch_token2id[PAD]])),
                   ' '.join(ids2seq(enc_inp[1, :], self.ch_id2token, PAD)),
                   ' '.join(map(lambda x: str(x), enc_inp[2, enc_inp[2, :] != self.ch_token2id[PAD]])),
                   ' '.join(ids2seq(enc_inp[2, :], self.ch_id2token, PAD)),
                   # ================================================
                   # ================================================
                   enc_len,
                   # ================================================
                   # ================================================
                   ' '.join(map(lambda x: str(x), dec_inp[0, dec_inp[0, :] != self.kr_token2id[PAD]])),
                   ' '.join(ids2seq(dec_inp[0, :], self.kr_id2token, PAD)),
                   ' '.join(map(lambda x: str(x), dec_inp[1, dec_inp[1, :] != self.kr_token2id[PAD]])),
                   ' '.join(ids2seq(dec_inp[1, :], self.kr_id2token, PAD)),
                   ' '.join(map(lambda x: str(x), dec_inp[2, dec_inp[2, :] != self.kr_token2id[PAD]])),
                   ' '.join(ids2seq(dec_inp[2, :], self.kr_id2token, PAD)),
                   # ================================================
                   # ================================================
                   ' '.join(map(lambda x: str(x), dec_trg[0, dec_trg[0, :] != self.kr_token2id[PAD]])),
                   ' '.join(ids2seq(dec_trg[0, :], self.kr_id2token, PAD)),
                   ' '.join(map(lambda x: str(x), dec_trg[1, dec_trg[1, :] != self.kr_token2id[PAD]])),
                   ' '.join(ids2seq(dec_trg[1, :], self.kr_id2token, PAD)),
                   ' '.join(map(lambda x: str(x), dec_trg[2, dec_trg[2, :] != self.kr_token2id[PAD]])),
                   ' '.join(ids2seq(dec_trg[2, :], self.kr_id2token, PAD)),
                   # ================================================
                   # ================================================
                   dec_len)
        return out_string

    def get_evaluation_configure(self, dataset, start_step,
                                 enc_inp, enc_len, dec_inp, dec_trg, dec_len):
        out_string = """
        =============================Evaluation Configure=============================
        Evaluation Step: %s
        Start Time: %s
        DataSet Description
        %s
        The Number of Data: %s
        Encoder Sequence Max Length: %s
        Decoder Sequence Max Length: %s
        Batch Size: %s

        Encoder Input Data
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################

        Encoder Sequence Length
        %s

        Decoder Input Data
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################

        Decoder Target Data
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################

        Decoder Sequence Length
        %s
        \r""" % (start_step,
                 time.strftime('%y-%m-%d %H:%M:%S'),
                 dataset.__doc__,
                 dataset.num_data,
                 self.enc_max_len,
                 self.dec_max_len,
                 self.batch_size,
                 # ================================================
                 # ================================================
                 ' '.join(map(lambda x: str(x), enc_inp[0, enc_inp[0, :] != self.ch_token2id[PAD]])),
                 ' '.join(ids2seq(enc_inp[0, :], self.ch_id2token, PAD)),
                 ' '.join(map(lambda x: str(x), enc_inp[1, enc_inp[1, :] != self.ch_token2id[PAD]])),
                 ' '.join(ids2seq(enc_inp[1, :], self.ch_id2token, PAD)),
                 ' '.join(map(lambda x: str(x), enc_inp[2, enc_inp[2, :] != self.ch_token2id[PAD]])),
                 ' '.join(ids2seq(enc_inp[2, :], self.ch_id2token, PAD)),
                 # ================================================
                 # ================================================
                 enc_len,
                 # ================================================
                 # ================================================
                 ' '.join(map(lambda x: str(x), dec_inp[0, dec_inp[0, :] != self.kr_token2id[PAD]])),
                 ' '.join(ids2seq(dec_inp[0, :], self.kr_id2token, PAD)),
                 ' '.join(map(lambda x: str(x), dec_inp[1, dec_inp[1, :] != self.kr_token2id[PAD]])),
                 ' '.join(ids2seq(dec_inp[1, :], self.kr_id2token, PAD)),
                 ' '.join(map(lambda x: str(x), dec_inp[2, dec_inp[2, :] != self.kr_token2id[PAD]])),
                 ' '.join(ids2seq(dec_inp[2, :], self.kr_id2token, PAD)),
                 # ================================================
                 # ================================================
                 ' '.join(map(lambda x: str(x), dec_trg[0, dec_trg[0, :] != self.kr_token2id[PAD]])),
                 ' '.join(ids2seq(dec_trg[0, :], self.kr_id2token, PAD)),
                 ' '.join(map(lambda x: str(x), dec_trg[1, dec_trg[1, :] != self.kr_token2id[PAD]])),
                 ' '.join(ids2seq(dec_trg[1, :], self.kr_id2token, PAD)),
                 ' '.join(map(lambda x: str(x), dec_trg[2, dec_trg[2, :] != self.kr_token2id[PAD]])),
                 ' '.join(ids2seq(dec_trg[2, :], self.kr_id2token, PAD)),
                 # ================================================
                 # ================================================
                 dec_len)
        return out_string


def my_test():
    os.environ['CUDA_VISIBLE_DEVICES'] = '0, 1'

    kr_token2id, kr_embed_vectors, kr_embed_dim = \
        load_word2vec('../Corpus/combined_1/word2vec/Korean_d300_w08_mc10.embed', [PAD, UNK, GO, EOS])
    ch_token2id, ch_embed_vectors, ch_embed_dim = \
        load_word2vec('../Corpus/combined_1/word2vec/Chinese_d300_w04_mc10.embed', [PAD, UNK])

    my_args = Arguments(cell_type='LSTM',
                        ch_token2id=ch_token2id,
                        kr_token2id=kr_token2id,
                        enc_hiddens=[512, 512, 512],
                        dec_hiddens=[1024, 1024],
                        output_drop=True,
                        state_drop=True,
                        ch_emb_dim=ch_embed_dim,
                        ch_emb_matrix=ch_embed_vectors,
                        kr_emb_dim=kr_embed_dim,
                        kr_emb_matrix=kr_embed_vectors,
                        projection_layer=1)

    my_model = Seq2SeqAttention('test', my_args)
    my_model.build_model(train_phase=True,
                         num_gpu=2,
                         batch_size=64,
                         enc_max_len=100,
                         dec_max_len=200,
                         gradients_clip=5.0,
                         beam_width=10,
                         alignment_history=False)

    session = tf.Session(config=get_tf_config())
    my_model.init_model(session)
    session.close()


if __name__ == '__main__':
    my_test()
