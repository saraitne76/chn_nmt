from models.seq2seq_attention import *
from models.bilstm_crf import *

os.environ['CUDA_VISIBLE_DEVICES'] = '0'

model_nmt = 'NMT_IM_CH05_KR05'
model_ner = 'NER_IM_CH05_ALL'

entity_dic = read_dic('Corpus/SilokCorpus/lexicon_silok.txt', 0)

graph_ner = tf.Graph()
with graph_ner.as_default():
    recognizer = BiLstmCrf(model_ner, args=None)
sess_ner = tf.Session(config=get_tf_config(), graph=graph_ner)
recognizer.init_model(sess=sess_ner, ckpt='step-100000')

graph_nmt = tf.Graph()
with graph_nmt.as_default():
    translator = Seq2SeqAttention(name=model_nmt, args=None)
    translator.build_model(train_phase=False,
                           num_gpu=1,
                           batch_size=256,
                           enc_max_len=200,
                           dec_max_len=200,
                           gradients_clip=None,
                           beam_width=10,
                           alignment_history=True)
sess_nmt = tf.Session(config=get_tf_config(), graph=graph_nmt)

test_data = read_pickles('Corpus/SilokCorpus/silok_15_10_im/test')

translator.eval_with_ner(sess_nmt=sess_nmt,
                         sess_ner=sess_ner,
                         ner_model=recognizer,
                         entity_dic=entity_dic,
                         data_list=test_data,
                         ckpt='step-350000',
                         test_log='step-350000_with_all_ner.txt',
                         num_examples=1000,
                         seed=777)

sess_ner.close()
sess_nmt.close()
