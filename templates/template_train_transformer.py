from transformer.transformer_wrapper import *


os.environ['CUDA_VISIBLE_DEVICES'] = '0, 1'

model = 'Transformer_NMT_IM_CH05_KR05'

params = model_params.BASE_PARAMS
params['batch_size'] = 42
token2id = read_vocab('transformer/vocab_with_vectors/ch05im_kr30_512.vocab.42739')
embedding = pickle_load('transformer/vocab_with_vectors/ch05im_kr30_512.vectors.42739')

translator = TransformerWrapper(model, params, token2id, embedding, num_gpu=2, train_phase=True)
sess = tf.Session(config=get_tf_config())

train_data = read_pickles('Corpus/SilokCorpus/silok_15_10_im/train')
translator.train(sess, train_data, 300000)
# test_data = read_pickles('Corpus/SilokCorpus/silok_15_10_im/test')
# translator.eval(sess, test_data, ckpt='step-100000', num_examples=500)

sess.close()
