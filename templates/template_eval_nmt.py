from models.seq2seq_attention import *

os.environ['CUDA_VISIBLE_DEVICES'] = '0'

model = 'M1_S1'  # modelID_splitID

# chinese_embed_model = 'Corpus/split_0/word2vec/Chinese_d300_w04_mc20.embed'
# korean_embed_model = 'Corpus/split_0/word2vec/Korean_d300_w08_mc20.embed'
# ch_word2id, ch_embed_vectors, ch_embed_dim = load_word2vec(chinese_embed_model, [PAD, UNK])
# kr_word2id, kr_embed_vectors, kr_embed_dim = load_word2vec(korean_embed_model, [PAD, UNK, GO, EOS])

# args = Arguments(cell_type='LSTM',
#                  ch_word2id=ch_word2id,
#                  kr_word2id=kr_word2id,
#                  enc_hiddens=[512, 512],
#                  output_drop=True,
#                  state_drop=True,
#                  ch_emb_dim=ch_embed_dim,
#                  ch_emb_matrix=ch_embed_vectors,
#                  kr_emb_dim=kr_embed_dim,
#                  kr_emb_matrix=kr_embed_vectors)
args = None

translator = Translator(name=model, args=args)

translator.build_model(train_phase=False,
                       num_gpu=1,
                       batch_size=256,
                       enc_max_len=100,
                       dec_max_len=200,
                       gradients_clip=None,
                       beam_width=10,
                       alignment_history=False)

test_data = read_pickles('Corpus/split_0/test')

sess = tf.Session(config=get_tf_config())

translator.eval(sess, test_data, ckpt='step-10000', num_examples=300, seed=777)

sess.close()
