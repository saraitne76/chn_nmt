from models.seq2seq_attention import *

os.environ['CUDA_VISIBLE_DEVICES'] = '0, 1'

model = 'NMT_IM_CH05_KR05'

chinese_embed_model = 'Corpus/SilokCorpus/silok_15_10_im/word2vec/Chinese_d500_w04_mc05/embedding.w2v'
korean_embed_model = 'Corpus/SilokCorpus/silok_15_10_im/word2vec/Korean_d500_w08_mc05/embedding.w2v'
ch_token2id, ch_embed_vectors, ch_embed_dim = load_word2vec(chinese_embed_model, [PAD, UNK])
kr_token2id, kr_embed_vectors, kr_embed_dim = load_word2vec(korean_embed_model, [PAD, UNK, GO, EOS])


args = Arguments(cell_type='LSTM',
                 ch_token2id=ch_token2id,
                 kr_token2id=kr_token2id,
                 enc_hiddens=[512, 512, 512],
                 dec_hiddens=[1024, 1024],
                 output_drop=True,
                 state_drop=True,
                 ch_emb_dim=ch_embed_dim,
                 ch_emb_matrix=ch_embed_vectors,
                 kr_emb_dim=kr_embed_dim,
                 kr_emb_matrix=kr_embed_vectors,
                 projection_layer=1)

translator = Seq2SeqAttention(name=model, args=args)

translator.build_model(train_phase=True,
                       num_gpu=2,
                       batch_size=52,
                       enc_max_len=200,
                       dec_max_len=200,
                       gradients_clip=None,
                       beam_width=None,
                       alignment_history=False)

train_data = read_pickles('Corpus/SilokCorpus/silok_15_10_im/train')

sess = tf.Session(config=get_tf_config())

translator.train(sess, train_data, 300000, 1e-3)
# translator.train(sess, train_data, 100000, 1e-4, ckpt='step-100000')

sess.close()
