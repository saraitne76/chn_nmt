from models.bilstm_crf import *

os.environ['CUDA_VISIBLE_DEVICES'] = '0'

model = 'NER_OH_CH05'

chinese_embed_model = 'Corpus/NERCorpus/silok_15_10_oh/word2vec/Chinese_d500_w04_mc05/embedding.w2v'
ch_token2id, ch_embed_vectors, ch_embed_dim = load_word2vec(chinese_embed_model, [PAD, UNK])


args = Arguments(cell_type='LSTM',
                 num_hiddens=[512, 512, 512],
                 output_drop=True,
                 state_drop=True,
                 char2id=ch_token2id,
                 char_embed_dim=ch_embed_dim,
                 char_embed_vectors=ch_embed_vectors)

translator = BiLstmCrf(name=model, args=args)


train_data = read_pickles('Corpus/NERCorpus/silok_15_10_oh/train')
test_data = read_pickles('Corpus/NERCorpus/silok_15_10_oh/test')

sess = tf.Session(config=get_tf_config())

translator.train(sess=sess,
                 train_step=100000,
                 lr=1e-3,
                 train_data=train_data,
                 max_seq_len=250,
                 test_data=test_data,
                 batch_size=128,
                 keep_prob=0.7)

sess.close()
