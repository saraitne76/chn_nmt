import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

from transformer import transformer_model               # noqa
from transformer import metrics                         # noqa
from transformer import model_params                    # noqa

from utils import *                                     # noqa
from tf_utils import *                                  # noqa
import time                                             # noqa
from nltk.translate.bleu_score import corpus_bleu       # noqa
from tensorflow.contrib.opt import LazyAdamOptimizer    # noqa


class TransformerWrapper:
    def __init__(self,
                 name: str,
                 params,
                 vocab: Dict,
                 embedding: Optional[np.ndarray],
                 num_gpu: int,
                 train_phase: bool):
        self.log_dir = os.path.join(LOG_DIR, name)
        self.param_dir = os.path.join(PARAMS_DIR, name)
        os.makedirs(self.log_dir, exist_ok=True)
        os.makedirs(self.param_dir, exist_ok=True)

        self.train_phase = train_phase
        self.num_gpu = num_gpu

        self.token2id = vocab
        self.id2token = dict_swap(vocab)

        self.params = params
        self.params['vocab_size'] = len(vocab)

        self.model = transformer_model.Transformer(self.params, self.train_phase, embedding)

        self.inputs = tf.placeholder(tf.int32,
                                     [self.params['batch_size'], self.params['max_len']],
                                     name='Inputs')

        self.global_step = tf.get_variable('GlobalStep', [],
                                           dtype=tf.int32,
                                           initializer=tf.initializers.constant(0),
                                           trainable=False)

        if self.train_phase:
            self.targets = tf.placeholder(tf.int32,
                                          [self.params['batch_size'], self.params['max_len']],
                                          name='Targets')

            learning_rate = get_learning_rate(learning_rate=params["learning_rate"],
                                              hidden_size=params["hidden_size"],
                                              learning_rate_warmup_steps=params["learning_rate_warmup_steps"],
                                              global_step=self.global_step)

            # Create optimizer. Use LazyAdamOptimizer from TF contrib, which is faster
            # than the TF core Adam optimizer.
            optimizer = LazyAdamOptimizer(learning_rate,
                                          beta1=params["optimizer_adam_beta1"],
                                          beta2=params["optimizer_adam_beta2"],
                                          epsilon=params["optimizer_adam_epsilon"])
            if self.num_gpu == 1:
                logits = self.model.build(self.inputs, self.targets)
                xentropy, weights = metrics.padded_cross_entropy_loss(
                    logits, self.targets, self.params['label_smoothing'], self.params['vocab_size'])
                loss = tf.reduce_sum(xentropy) / tf.reduce_sum(weights)
                tf.identity(loss, "cross_entropy")
                self._loss, self._loss_op = tf.metrics.mean(loss, name='LossMean')
                grads_and_vars = optimizer.compute_gradients(loss, colocate_gradients_with_ops=True)
                self.train_op = get_train_op(grads_and_vars, optimizer, self.global_step)
            else:
                if (self.params['batch_size'] % self.num_gpu) != 0:
                    raise ValueError('batch_size: %d, num_gpu: %d' % (self.params['batch_size'], self.num_gpu))
                split_inputs = tf.split(self.inputs, self.num_gpu)
                split_targets = tf.split(self.targets, self.num_gpu)

                # tower_batch = self.params['batch_size'] // self.num_gpu
                tower_loss = []
                tower_gradients = []

                for i in range(self.num_gpu):
                    with tf.device('/gpu:%d' % i):
                        with tf.name_scope('tower_%d' % i):
                            with tf.variable_scope(tf.get_variable_scope(), reuse=tf.AUTO_REUSE):
                                logits = self.model.build(split_inputs[i], split_targets[i])
                                xentropy, weights = metrics.padded_cross_entropy_loss(logits,
                                                                                      split_targets[i],
                                                                                      self.params['label_smoothing'],
                                                                                      self.params['vocab_size'])
                                loss = tf.reduce_sum(xentropy) / tf.reduce_sum(weights)
                                tower_loss.append(loss)
                                grads_and_vars = optimizer.compute_gradients(loss, colocate_gradients_with_ops=True)
                                tower_gradients.append(grads_and_vars)

                total_loss = tf.reduce_mean(tower_loss)
                self._loss, self._loss_op = tf.metrics.mean(total_loss, name='LossMean')
                total_grad = merge_gradients(tower_gradients)
                self.train_op = optimizer.apply_gradients(total_grad, global_step=self.global_step)

            summaries = []
            with tf.name_scope('General'):
                summaries.append(tf.summary.scalar('Loss', self._loss))
                summaries.append(tf.summary.scalar('LearningRate', learning_rate))
            self.merged = tf.summary.merge(summaries)

        else:
            self.pred = self.model.build(self.inputs, targets=None)

        self.saver = tf.train.Saver()
        self.get_trainable_var = tf.trainable_variables()
        self.global_var_init = tf.global_variables_initializer()
        self.local_var_init = tf.local_variables_initializer()
        return

    def build_dataset(self, data_list: List[PairData]):
        dataset = DataSet(enc_max_len=self.params['max_len'],
                          dec_max_len=self.params['max_len'],
                          ch_word2id=self.token2id,
                          kr_word2id=self.token2id)
        dataset.make(pairs=data_list,
                     train_phase=self.train_phase)
        return dataset

    def init_model(self, sess, log_file=None, ckpt=None, variables_rewrite=False):
        if ckpt:
            self.saver.restore(sess, os.path.join(self.param_dir, ckpt, PARAMS))
            if log_file:
                print_write('model loaded from file: %s\n' % os.path.join(self.log_dir, ckpt),
                            os.path.join(self.log_dir, log_file), 'a')
        else:
            sess.run(self.global_var_init)
            if log_file:
                with open(os.path.join(self.log_dir, log_file), 'w') as f:
                    print_write('global variable initialize\n', f)

            writer = tf.summary.FileWriter(os.path.join(self.log_dir, 'train'), filename_suffix='-graph')
            writer.add_graph(sess.graph)
            writer.close()

        if not ckpt or variables_rewrite:
            with open(os.path.join(self.log_dir, CONFIG_TXT), 'w') as f:
                print_write(model_params.print_params(self.params), f)
                count_vars = 0
                for var in self.get_trainable_var:
                    name = var.name
                    shape = var.shape.as_list()
                    num_elements = var.shape.num_elements()
                    print_write('Variable name: %s\n' % name, f)
                    print_write('Placed device: %s\n' % var.device, f)
                    print_write('Shape : %s  Elements: %d\n' % (str(shape), num_elements), f)
                    print_write('===============================================================================\n', f)
                    count_vars = count_vars + num_elements
                print_write('Total number of trainable variables %d\n' % count_vars, f)
                print_write('===============================================================================\n', f)
        return

    def get_train_feed(self, dataset):
        enc_inp, enc_len, dec_inp, dec_trg, dec_len = dataset.get_train_batch(self.params['batch_size'])
        feed = {self.inputs: enc_inp,
                self.targets: dec_trg}
        return feed

    def train(self,
              sess: tf.Session,
              data_list: List[PairData],
              train_step: int,
              ckpt=None,
              summary_step=1000,
              variable_rewrite=False):
        assert (train_step % summary_step) == 0, 'train_step: %d, summary_step: %d' % (train_step, summary_step)

        dataset = self.build_dataset(data_list)

        self.init_model(sess, TRAIN_LOG, ckpt, variable_rewrite)
        global_step = sess.run(self.global_step)
        start_step = global_step
        sess.run(self.local_var_init)
        with open(os.path.join(self.log_dir, TRAIN_LOG), 'a') as f:
            print_write(self.get_running_configure(dataset, start_step, train_step), f)

        s = time.time()
        for i in range(train_step // (summary_step * 10)):
            writer = tf.summary.FileWriter(os.path.join(self.log_dir, 'train'),
                                           filename_suffix='-step-%d' % global_step)
            for j in range(summary_step * 10):
                feed = self.get_train_feed(dataset)
                _, loss = sess.run([self.train_op, self._loss_op], feed_dict=feed)
                global_step = sess.run(self.global_step)

                print('\rTraining - Loss: %0.3f, step %d/%d'
                      % (loss, global_step, train_step + start_step), end='')

                if global_step % summary_step == 0:
                    merged, loss = sess.run([self.merged, self._loss])
                    writer.add_summary(merged, global_step)

                    print('\r', end='')
                    print_write('Training - Loss: %0.3f, step: %d, %0.2f sec/step\n'
                                % (loss, global_step, (time.time() - s) / summary_step),
                                os.path.join(self.log_dir, TRAIN_LOG), 'a')

                    s = time.time()
                    sess.run(self.local_var_init)

            print_write('global step: %d, model save, time: %s\n'
                        % (global_step, time.strftime('%y-%m-%d %H:%M:%S')),
                        os.path.join(self.log_dir, TRAIN_LOG), 'a')
            writer.close()
            self.saver.save(sess, os.path.join(self.param_dir, 'step-%d' % global_step, PARAMS))

            s = time.time()
        return

    def get_eval_feed(self, dataset):
        enc_tokens, enc_inp, enc_len, dec_tokens = dataset.get_eval_batch(self.params['batch_size'])
        n_real_data = enc_inp.shape[0]
        end = False
        if dataset.num_data == dataset.sequential_indices:
            end = True
        elif dataset.num_data < dataset.sequential_indices:
            end = True
            fill_size = self.params['batch_size'] - n_real_data
            enc_inp = np.concatenate([enc_inp, np.zeros((fill_size, self.params['max_len']), dtype=np.uint16)], axis=0)
        else:
            pass
        feed = {self.inputs: enc_inp}
        return feed, enc_tokens, dec_tokens, n_real_data, end

    def eval_init(self,
                  sess: tf.Session,
                  data_list: List[PairData],
                  ckpt='step-',
                  test_log=None,
                  variable_rewrite=False):
        if test_log is None:
            test_log = ckpt + '.txt'

        dataset = self.build_dataset(data_list)
        self.init_model(sess, log_file=None, ckpt=ckpt, variables_rewrite=variable_rewrite)
        global_step = sess.run(self.global_step)

        with open(os.path.join(self.log_dir, test_log), 'w') as f:
            print_write(self.get_running_configure(dataset, global_step, None), f)

        sess.run(self.local_var_init)
        return dataset, test_log

    def eval(self,
             sess: tf.Session,
             data_list: List[PairData],
             ckpt='step-',
             test_log=None,
             num_examples=5,
             seed=777,
             variable_rewrite=False):
        dataset, test_log = self.eval_init(sess, data_list, ckpt, test_log, variable_rewrite)

        src = []
        ref = []
        hyp = []
        dataset.sequential_indices = 0
        cnt = 0
        end = False
        while not end:
            feed, source, target, n_real_data, end = self.get_eval_feed(dataset)
            cnt += n_real_data
            out = sess.run(self.pred, feed_dict=feed)
            prediction = out['outputs']

            for i in range(n_real_data):
                src.append(source[i])
                ref.append([target[i]])
                hyp.append(ids2seq(prediction[i, :], self.id2token, EOS))

            print('\rEvaluation... %d/%d' % (cnt, dataset.num_data), end='')
        print()
        self.print_eval_result(src, ref, hyp, test_log, num_examples, seed)
        return

    def eval_with_ner(self,
                      sess_nmt: tf.Session,
                      sess_ner: tf.Session,
                      ner_model,
                      entity_dic: Dict,
                      data_list: List[PairData],
                      ckpt='step-',
                      test_log=None,
                      num_examples=5,
                      seed=777,
                      variable_rewrite=False):
        dataset, test_log = self.eval_init(sess_nmt, data_list, ckpt, test_log, variable_rewrite)

        src = []
        ref = []
        hyp = []
        hyp_m = []
        nes = []
        dataset.sequential_indices = 0
        cnt = 0
        end = False
        while not end:
            feed, source, target, n_real_data, end = self.get_eval_feed(dataset)
            cnt += n_real_data

            out = sess_nmt.run(self.pred, feed_dict=feed)
            prediction = out['outputs']
            att_maps = np.mean(out['att_w'], axis=1)
            # [batch, layers, heads, output_len, input_len]

            src_temp = []
            hyp_temp = []
            for i in range(n_real_data):
                src_temp.append(source[i])
                hyp_temp.append(ids2seq(prediction[i, :], self.id2token, EOS))

            entities = ner_model.runs(sess_ner, src_temp, n_real_data)
            for i in range(n_real_data):
                if len(hyp_temp[i]) > 0:
                    att_map = att_maps[i, :len(hyp_temp[i]), :len(src_temp[i])]
                    ne_temp = entity_lookup(att_map, entities[i], hyp_temp[i])
                    hyp_m.append(entity_replace(hyp_temp[i], ne_temp, entity_dic))
                    ref.append([target[i]])
                    nes.append(ne_temp)
                else:
                    hyp_m.append(hyp_temp[i])
                    ref.append([target[i]])
                    nes.append([])

            src.extend(src_temp)
            hyp.extend(hyp_temp)

            print('\rEvaluation... %d/%d' % (cnt, dataset.num_data), end='')
        print()

        self.print_eval_result_with_ner(src, ref, hyp, hyp_m, nes, test_log, num_examples, seed)
        return

    def run_test(self,
                 sess: tf.Session,
                 data,
                 ckpt='step-'):
        self.init_model(sess, log_file=None, ckpt=ckpt)
        inputs = np.zeros([1, self.params['max_len']], dtype=np.uint16)
        # inputs[0, :data.ch_seq_len] = seq2ids(data.ch_tok, self.token2id)
        seq_len = len(data)
        inputs[0, :seq_len] = seq2ids(list(data), self.token2id)
        feed = {self.inputs: inputs}
        # out, att_w = sess.run([self.pred, tf.get_collection('attention_weights')], feed_dict=feed)
        out = sess.run(self.pred, feed_dict=feed)
        # print(data.ch_tok)
        # print(data.kr_tok)
        print(ids2seq(out['outputs'][0], self.id2token, EOS))
        return out

    def print_eval_result(self, source, references, hypotheses, log, num_examples, seed):
        t = 'len(src) = %d, len(ref) = %d, len(hyp) = %d' % (len(source), len(references), len(hypotheses))
        assert len(source) == len(references) == len(hypotheses), t

        num_results = len(hypotheses)

        s = time.time()
        bleu = corpus_bleu(references, hypotheses)
        bleu_calc_time = time.time() - s
        with open(os.path.join(self.log_dir, log), 'a') as f:
            s = """
            End Time: %s
            Bleu-4 Score: %0.4f
            Bleu-4 calculation time: %0.2f
            The number of predictions: %d
            Examples
            """ % (time.strftime('%y-%m-%d %H:%M:%S'), bleu, bleu_calc_time, num_results)
            print_write(s, f)
            random.seed(seed)        # Always print same examples of result
            for i in range(num_examples):
                n = random.randint(0, num_results-1)
                s = """
                    source[%d]: %s
                 reference[%d]: %s
                hypothesis[%d]: %s
                """ % (n, ' '.join(source[n]), n, ' '.join(references[n][0]), n, ' '.join(hypotheses[n]))
                print_write(s, f)
        return

    def print_eval_result_with_ner(self,
                                   source,
                                   references,
                                   hypotheses,
                                   hypotheses_modified,
                                   entities,
                                   log,
                                   num_examples,
                                   seed):
        t = 'len(src) = %d, len(ref) = %d, len(hyp) = %d' % (len(source), len(references), len(hypotheses))
        assert len(source) == len(references) == len(hypotheses), t
        # print(hypotheses_modified)
        num_results = len(hypotheses)

        s = time.time()
        bleu = corpus_bleu(references, hypotheses)
        bleu_m = corpus_bleu(references, hypotheses_modified)
        bleu_calc_time = time.time() - s
        with open(os.path.join(self.log_dir, log), 'a') as f:
            s = """
            End Time: %s
              Normal Bleu-4 Score: %0.4f
            Modified Bleu-4 Score: %0.4f
            Bleu-4 calculation time: %0.2f
            The number of predictions: %d
            Examples
            """ % (time.strftime('%y-%m-%d %H:%M:%S'), bleu, bleu_m, bleu_calc_time, num_results)
            print_write(s, f)
            random.seed(seed)        # Always print same examples of result
            for i in range(num_examples):
                n = random.randint(0, num_results-1)
                s = """
                    source[%d]: %s
                 reference[%d]: %s
                hypothesis[%d]: %s
                   hyp_mod[%d]: %s
                  entities[%d]: %s
                """ % (n, ' '.join(source[n]),
                       n, ' '.join(references[n][0]),
                       n, ' '.join(hypotheses[n]),
                       n, ' '.join(hypotheses_modified[n]),
                       n, entities[n])
                print_write(s, f)
        return

    def get_running_configure(self,
                              dataset,
                              start_step: Optional[int],
                              train_step: Optional[int]):
        enc_inp, enc_len, dec_inp, dec_trg, dec_len = dataset.get_train_batch(batch_size=3)
        if self.train_phase:
            out_string = self.get_training_configure(dataset, start_step, train_step,
                                                     enc_inp, enc_len, dec_trg, dec_len)
        else:
            out_string = self.get_evaluation_configure(dataset, start_step,
                                                       enc_inp, enc_len, dec_trg, dec_len)
        return out_string

    def get_training_configure(self, dataset, start_step, train_step,
                               enc_inp, enc_len, dec_trg, dec_len):
        out_string = """
        ==============================Training Configure==============================
        Training Step: %s ~ %s
        The Number of GPU: %s
        Start Time: %s
        DataSet Description
        %s
        The Number of Data: %s
        Encoder Sequence Max Length: %s
        Decoder Sequence Max Length: %s
        Batch Size: %s

        Input Data
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################

        Encoder Sequence Length
        %s

        Target Data
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################

        Decoder Sequence Length
        %s
        ==============================================================================
        \n\r""" % (start_step,
                   start_step + train_step,
                   self.num_gpu,
                   time.strftime('%y-%m-%d %H:%M:%S'),
                   dataset.__doc__,
                   dataset.num_data,
                   self.params['max_len'],
                   self.params['max_len'],
                   self.params['batch_size'],
                   # ================================================
                   # ================================================
                   ' '.join(map(lambda x: str(x), enc_inp[0, enc_inp[0, :] != self.token2id[PAD]])),
                   ' '.join(ids2seq(enc_inp[0, :], self.id2token, PAD)),
                   ' '.join(map(lambda x: str(x), enc_inp[1, enc_inp[1, :] != self.token2id[PAD]])),
                   ' '.join(ids2seq(enc_inp[1, :], self.id2token, PAD)),
                   ' '.join(map(lambda x: str(x), enc_inp[2, enc_inp[2, :] != self.token2id[PAD]])),
                   ' '.join(ids2seq(enc_inp[2, :], self.id2token, PAD)),
                   # ================================================
                   # ================================================
                   enc_len,
                   # ================================================
                   # ================================================
                   ' '.join(map(lambda x: str(x), dec_trg[0, dec_trg[0, :] != self.token2id[PAD]])),
                   ' '.join(ids2seq(dec_trg[0, :], self.id2token, PAD)),
                   ' '.join(map(lambda x: str(x), dec_trg[1, dec_trg[1, :] != self.token2id[PAD]])),
                   ' '.join(ids2seq(dec_trg[1, :], self.id2token, PAD)),
                   ' '.join(map(lambda x: str(x), dec_trg[2, dec_trg[2, :] != self.token2id[PAD]])),
                   ' '.join(ids2seq(dec_trg[2, :], self.id2token, PAD)),
                   # ================================================
                   # ================================================
                   dec_len)
        return out_string

    def get_evaluation_configure(self, dataset, start_step,
                                 enc_inp, enc_len, dec_trg, dec_len):
        out_string = """
        =============================Evaluation Configure=============================
        Evaluation Step: %s
        Start Time: %s
        DataSet Description
        %s
        The Number of Data: %s
        Encoder Sequence Max Length: %s
        Decoder Sequence Max Length: %s
        Batch Size: %s

        Encoder Input Data
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################

        Encoder Sequence Length
        %s

        Decoder Target Data
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################
        %s
        %s
        #####################################################

        Decoder Sequence Length
        %s
        \r""" % (start_step,
                 time.strftime('%y-%m-%d %H:%M:%S'),
                 dataset.__doc__,
                 dataset.num_data,
                 self.params['max_len'],
                 self.params['max_len'],
                 self.params['batch_size'],
                 # ================================================
                 # ================================================
                 ' '.join(map(lambda x: str(x), enc_inp[0, enc_inp[0, :] != self.token2id[PAD]])),
                 ' '.join(ids2seq(enc_inp[0, :], self.id2token, PAD)),
                 ' '.join(map(lambda x: str(x), enc_inp[1, enc_inp[1, :] != self.token2id[PAD]])),
                 ' '.join(ids2seq(enc_inp[1, :], self.id2token, PAD)),
                 ' '.join(map(lambda x: str(x), enc_inp[2, enc_inp[2, :] != self.token2id[PAD]])),
                 ' '.join(ids2seq(enc_inp[2, :], self.id2token, PAD)),
                 # ================================================
                 # ================================================
                 enc_len,
                 # ================================================
                 # ================================================
                 ' '.join(map(lambda x: str(x), dec_trg[0, dec_trg[0, :] != self.token2id[PAD]])),
                 ' '.join(ids2seq(dec_trg[0, :], self.id2token, PAD)),
                 ' '.join(map(lambda x: str(x), dec_trg[1, dec_trg[1, :] != self.token2id[PAD]])),
                 ' '.join(ids2seq(dec_trg[1, :], self.id2token, PAD)),
                 ' '.join(map(lambda x: str(x), dec_trg[2, dec_trg[2, :] != self.token2id[PAD]])),
                 ' '.join(ids2seq(dec_trg[2, :], self.id2token, PAD)),
                 # ================================================
                 # ================================================
                 dec_len)
        return out_string


def get_learning_rate(learning_rate, hidden_size, learning_rate_warmup_steps, global_step):
    """Calculate learning rate with linear warmup and rsqrt decay."""
    with tf.name_scope("learning_rate"):
        warmup_steps = tf.to_float(learning_rate_warmup_steps)
        step = tf.to_float(global_step)

        learning_rate *= (hidden_size ** -0.5)
        # Apply linear warmup
        learning_rate *= tf.minimum(1.0, step / warmup_steps)
        # Apply rsqrt decay
        learning_rate *= tf.rsqrt(tf.maximum(step, warmup_steps))

        # Create a named tensor that will be logged using the logging hook.
        # The full name includes variable and names scope. In this case, the name
        # is model/get_train_op/learning_rate/learning_rate
        tf.identity(learning_rate, "learning_rate")

        return learning_rate


def get_train_op(grads_and_vars, optimizer, global_step):
    """Generate training op."""
    # Apply gradients using LazyAdamOptimizer.
    minimize_op = optimizer.apply_gradients(
        grads_and_vars, global_step=global_step, name="train_op")
    update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
    train_op = tf.group(minimize_op, update_ops)
    return train_op


def read_vocab(path):
    f = open(path, 'r', encoding='utf-8')
    lines = f.readlines()
    f.close()
    dic = dict()
    for i, l in enumerate(lines):
        try:
            s = l.split('\t')
            key = s[0].strip('$\n')
            value = int(s[1].strip('$\n'))
            dic[key] = value
        except IndexError:
            print(i)
            print(lines[i])
            print(lines[i - 1])
            raise IndexError
    return dic


def my_test():
    os.environ['CUDA_VISIBLE_DEVICES'] = '0, 1'

    params = model_params.BASE_PARAMS
    token2id = read_vocab('vocab/ch05_kr05.vocab.87069')

    my_model = TransformerWrapper('test', params, token2id, train_phase=True, num_gpu=2)

    session = tf.Session(config=get_tf_config())
    my_model.init_model(session)
    session.close()


if __name__ == '__main__':
    my_test()
