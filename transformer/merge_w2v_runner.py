import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

dest = 'vocab_with_vectors'

c256_05 = '../Corpus/SilokCorpus/silok_15_10_im/word2vec/Chinese_d256_w04_mc05/embedding.w2v'
c512_05 = '../Corpus/SilokCorpus/silok_15_10_im/word2vec/Chinese_d512_w04_mc05/embedding.w2v'

k256_05 = '../Corpus/SilokCorpus/silok_15_10_im/word2vec/Korean_d256_w08_mc05/embedding.w2v'
k256_30 = '../Corpus/SilokCorpus/silok_15_10_im/word2vec/Korean_d256_w08_mc30/embedding.w2v'

k512_05 = '../Corpus/SilokCorpus/silok_15_10_im/word2vec/Korean_d512_w08_mc05/embedding.w2v'
k512_30 = '../Corpus/SilokCorpus/silok_15_10_im/word2vec/Korean_d512_w08_mc30/embedding.w2v'

# ch05im_kr05_256
os.system('python merge_w2v.py -c %s -k %s -t %s/ch05im_kr05_256' % (c256_05, k256_05, dest))

# ch05im_kr30_256
os.system('python merge_w2v.py -c %s -k %s -t %s/ch05im_kr30_256' % (c256_05, k256_30, dest))

# ch05im_kr05_512
os.system('python merge_w2v.py -c %s -k %s -t %s/ch05im_kr05_512' % (c512_05, k512_05, dest))

# ch05im_kr30_512
os.system('python merge_w2v.py -c %s -k %s -t %s/ch05im_kr30_512' % (c512_05, k512_30, dest))
