# Copyright 2018 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Defines Transformer model parameters."""
import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

from collections import defaultdict     # noqa

BASE_PARAMS = defaultdict(
    # lambda: None,  # Set default value to None.

    # Input params
    # batch_size=42,  # Maximum number of tokens per batch of examples.
    max_len=200,  # Maximum number of tokens per example.

    # Model params
    initializer_gain=1.0,  # Used in trainable variable initialization.
    hidden_size=512,  # Model dimension in the hidden layers.
    num_hidden_layers=6,  # Number of layers in the encoder and decoder stacks.
    num_heads=8,  # Number of heads to use in multi-headed attention.
    filter_size=2048,  # Inner layer dimension in the feedforward network.

    # Dropout values (only used when training)
    layer_postprocess_dropout=0.1,
    attention_dropout=0.1,
    relu_dropout=0.1,

    # Training params
    label_smoothing=0.1,
    learning_rate=2.0,
    learning_rate_decay_rate=1.0,
    learning_rate_warmup_steps=16000,

    # Optimizer params
    optimizer_adam_beta1=0.9,
    optimizer_adam_beta2=0.997,
    optimizer_adam_epsilon=1e-09,

    # Default prediction params
    extra_decode_length=100,
    beam_size=4,
    alpha=0.6,  # used to calculate length normalization in beam search
    allow_ffn_pad=True
)

BIG_PARAMS = BASE_PARAMS.copy()
BIG_PARAMS.update(
    hidden_size=1024,
    filter_size=4096,
    num_heads=16,
)

# Parameters for testing the model
TINY_PARAMS = BASE_PARAMS.copy()
TINY_PARAMS.update(
    hidden_size=128,
    num_heads=2,
    filter_size=256,
    num_hidden_layers=2,
)


BASE2_PARAMS = BASE_PARAMS.copy()
BASE2_PARAMS.update(
    num_hidden_layers=4
)


SMALL_PARAMS = BASE_PARAMS.copy()
SMALL_PARAMS.update(
    hidden_size=256,
    num_heads=4,
    filter_size=1024,
)


SMALL2_PARAMS = BASE_PARAMS.copy()
SMALL2_PARAMS.update(
    num_hidden_layers=4,
    hidden_size=256,
    num_heads=4,
    filter_size=1024,
)


SMALL3_PARAMS = BASE_PARAMS.copy()
SMALL3_PARAMS.update(
    num_hidden_layers=3,
    hidden_size=256,
    num_heads=4,
    filter_size=1024,
)


SMALL4_PARAMS = BASE_PARAMS.copy()
SMALL4_PARAMS.update(
    num_hidden_layers=2,
    hidden_size=256,
    num_heads=4,
    filter_size=1024,
)


def print_params(params):
    s = """
        ===============Model Hyper Parameter===============
        # Model params
        hidden_size: %s
        num_hidden_layers: %s
        num_heads : %s
        filter_size : %s
        ===================================================
        \n\r""" % (params['hidden_size'],
                   params['num_hidden_layers'],
                   params['num_heads'],
                   params['filter_size'])
    return s
