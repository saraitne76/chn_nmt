import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

from utils import *                 # noqa
import argparse                     # noqa


def main(ch_word2vec: str, kr_word2vec: str, target_path: str):
    os.makedirs(os.path.dirname(target_path), exist_ok=True)

    special_words = [PAD, UNK, GO, EOS]

    w2v_ch = Word2Vec.load(ch_word2vec)
    w2v_kr = Word2Vec.load(kr_word2vec)

    ch_vector_dim = w2v_ch.wv.vector_size
    kr_vector_dim = w2v_kr.wv.vector_size
    if ch_vector_dim != kr_vector_dim:
        raise ValueError('Chinese vector dim %d, Korean vector dim %d' % (ch_vector_dim, kr_vector_dim))
    else:
        size = ch_vector_dim

    ch_token2id = w2v_ch.wv.index2word
    kr_token2id = w2v_kr.wv.index2word

    ch_vectors = w2v_ch.wv.vectors
    kr_vectors = w2v_kr.wv.vectors
    # print(special_words, type(special_words))
    # print(ch_token2id[:15], type(ch_token2id), len(ch_token2id))
    # print(kr_token2id[:15], type(kr_token2id), len(kr_token2id))

    integrated = special_words + ch_token2id + kr_token2id

    # duplicated tokens are removed while dictionary is made.
    # But duplicated token ID are also removed -> ID gap [1, 2, 3 ... 21, 22, 23 ...]
    token2idx = {c: i for i, c in enumerate(integrated)}
    token2idx = list(token2idx.items())
    token2idx.sort(key=lambda k: k[1])

    new_token2idx = []
    # token ID re-assign
    for i, item in enumerate(token2idx):
        new_token2idx.append((item[0], i))

    new_vocab_size = len(new_token2idx)

    m = np.mean(np.concatenate((ch_vectors, kr_vectors), axis=0))
    s = np.std(np.concatenate((ch_vectors, kr_vectors), axis=0))
    vectors = np.zeros((new_vocab_size, size), dtype=np.float32)
    for i, item in enumerate(new_token2idx):
        key = item[0]
        value = item[1]
        if key in special_words:
            if key == PAD:
                vectors[value:] = np.zeros((1, size))
            else:
                vectors[value:] = np.random.normal(size=(1, size), loc=m, scale=s)
        else:
            try:
                vectors[value:] = w2v_ch.wv.get_vector(key)
            except KeyError:
                vectors[value:] = w2v_kr.wv.get_vector(key)
        print('\r%d/%d' % (i, new_vocab_size), end='')

    with open(target_path + '.%s.%s' % ('vocab', new_vocab_size), 'w', encoding='utf-8') as f:
        for key, value in new_token2idx:
            f.write('$%s$\t$%s$\n' % (key, value))

    pickle_store(vectors, target_path + '.%s.%s' % ('vectors', new_vocab_size))
    return


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('-c', '--cw2v', type=str,
                        help='Chinese word2vec file path')
    parser.add_argument('-k', '--kw2v', type=str,
                        help='Korean word2vec file path')
    parser.add_argument('-t', '--target', type=str,
                        help='Target file path')
    args = parser.parse_args()

    main(args.cw2v, args.kw2v, args.target)

