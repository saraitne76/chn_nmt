from utils import *
import time


kr_word2id, kr_embed_vectors, kr_embed_dim = load_word2vec('Corpus/split_0/word2vec/Korean_d300_w08_mc20.embed',
                                                           [PAD, UNK, GO, EOS])
ch_word2id, ch_embed_vectors, ch_embed_dim = load_word2vec('Corpus/split_0/word2vec/Chinese_d300_w04_mc20.embed',
                                                           [PAD, UNK])

s = time.time()
data_list = read_pickles('Corpus/split_0/train')
print('number of data: %d' % len(data_list))
print('read time: %f' % (time.time()-s))

s = time.time()
dataset = DataSet(enc_max_len=100,
                  dec_max_len=200,
                  ch_word2id=ch_word2id,
                  kr_word2id=kr_word2id)

dataset.make(pairs=data_list)

print('dataset build time: %s' % (time.time()-s))


enc_inp, enc_len, dec_inp, dec_trg, dec_len = dataset.get_batch(batch_size=3)
s = """
    ==============================Training Configure==============================
    The Number of Data: %s
    Encoder Sequence Max Length: %s
    Decoder Sequence Max Length: %s
    Encoder Input Data
    %s
    Encoder Sequence Length
    %s
    Decoder Input Data
    %s
    Decoder Target Data
    %s
    Decoder Sequence Length
    %s
    ==============================================================================
    \n\r""" % (dataset.num_data,
               dataset.enc_max_len,
               dataset.dec_max_len,
               enc_inp,
               enc_len,
               dec_inp,
               dec_trg,
               dec_len)
print(s)
