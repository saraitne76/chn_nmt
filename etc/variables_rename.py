import tensorflow as tf
import argparse
import os
from typing import List


def rename(load_ckpt_dir, replace_from: List[str], replace_to: List[str], practice):
    # checkpoint = tf.train.get_checkpoint_state(load_ckpt_dir)
    with tf.Session() as sess:
        for var_name, _ in tf.contrib.framework.list_variables(load_ckpt_dir):
            # Load the variable
            var = tf.contrib.framework.load_variable(load_ckpt_dir, var_name)

            # Set the new name
            new_name = var_name
            for from_, to_ in zip(replace_from, replace_to):
                if from_ in var_name:
                    new_name = new_name.replace(from_, to_)

            var = tf.Variable(var, name=new_name)
            # Rename the variable
            print('From [%s] To [%s]' % (var_name, var.name))

        if not practice:
            # Save the variables
            saver = tf.train.Saver()
            sess.run(tf.global_variables_initializer())
            path = saver.save(sess, os.path.join(load_ckpt_dir+'_renamed', 'params'))
            print('saved: %s' % path)


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-l', '--load_dir', type=str, default='Params/M1_S1/step-0000',
                        help='Load checkpoint file directory')
    parser.add_argument('-f', '--replace_from', type=str, default='from_a/from_b',
                        help='replace FROM name')
    parser.add_argument('-t', '--replace_to', type=str, default='to_a/to_b',
                        help='replace TO name')
    parser.add_argument('-p', '--practice', type=int, default=1,
                        help='practice: 1, real run: 0')
    args = parser.parse_args()

    if args.practice == 1:
        p = True
    elif args.practice == 0:
        p = False
    else:
        raise ValueError('practice: %d' % args.practice)

    replace_from = args.replace_from.split('/')
    replace_to = args.replace_to.split('/')

    rename(load_ckpt_dir=args.load_dir,
           replace_from=replace_from,
           replace_to=replace_to,
           practice=p)


if __name__ == '__main__':
    main()
