from transformer.transformer_wrapper import *


def get_running_configure(dataset, token2id, id2token):
    input_tokens, input_ids, input_len, target_tokens, target_ids1, target_ids2, target_len = dataset.get_check_batch(3)
    out_string = get_evaluation_configure(input_tokens, input_ids, input_len, target_tokens, target_ids1, target_ids2,
                                          target_len, token2id, id2token)
    return out_string


def get_evaluation_configure(input_tokens, input_ids, input_len, target_tokens, target_ids1, target_ids2, target_len,
                             token2id, id2token):
    out_string = """
    =============================Evaluation Configure=============================
    Encoder Input Data
    #####################################################
    %s
    %s
    %s
    #####################################################
    %s
    %s
    %s
    #####################################################
    %s
    %s
    %s
    #####################################################

    Encoder Sequence Length
    %s

    Decoder Target Data
    #####################################################
    %s
    %s
    %s
    #####################################################
    %s
    %s
    %s
    #####################################################
    %s
    %s
    %s
    #####################################################

    Decoder Target Data
    #####################################################
    %s
    %s
    %s
    #####################################################
    %s
    %s
    %s
    #####################################################
    %s
    %s
    %s
    #####################################################

    Decoder Sequence Length
    %s
    \r""" % (  # ================================================
        # ================================================
        ' '.join(map(lambda x: str(x), input_ids[0, input_ids[0, :] != token2id[PAD]])),
        ' '.join(ids2seq(input_ids[0, :], id2token, PAD)),
        ' '.join(input_tokens[0]),
        ' '.join(map(lambda x: str(x), input_ids[1, input_ids[1, :] != token2id[PAD]])),
        ' '.join(ids2seq(input_ids[1, :], id2token, PAD)),
        ' '.join(input_tokens[1]),
        ' '.join(map(lambda x: str(x), input_ids[2, input_ids[2, :] != token2id[PAD]])),
        ' '.join(ids2seq(input_ids[2, :], id2token, PAD)),
        ' '.join(input_tokens[2]),
        # ================================================
        # ================================================
        input_len,
        # ================================================
        # ================================================
        ' '.join(map(lambda x: str(x), target_ids1[0, target_ids1[0, :] != token2id[PAD]])),
        ' '.join(ids2seq(target_ids1[0, :], id2token, PAD)),
        ' '.join(target_tokens[0]),
        ' '.join(map(lambda x: str(x), target_ids1[1, target_ids1[1, :] != token2id[PAD]])),
        ' '.join(ids2seq(target_ids1[1, :], id2token, PAD)),
        ' '.join(target_tokens[1]),
        ' '.join(map(lambda x: str(x), target_ids1[2, target_ids1[2, :] != token2id[PAD]])),
        ' '.join(ids2seq(target_ids1[2, :], id2token, PAD)),
        ' '.join(target_tokens[2]),
        # ================================================
        # ================================================
        ' '.join(map(lambda x: str(x), target_ids2[0, target_ids2[0, :] != token2id[PAD]])),
        ' '.join(ids2seq(target_ids2[0, :], id2token, PAD)),
        ' '.join(target_tokens[0]),
        ' '.join(map(lambda x: str(x), target_ids2[1, target_ids2[1, :] != token2id[PAD]])),
        ' '.join(ids2seq(target_ids2[1, :], id2token, PAD)),
        ' '.join(target_tokens[1]),
        ' '.join(map(lambda x: str(x), target_ids2[2, target_ids2[2, :] != token2id[PAD]])),
        ' '.join(ids2seq(target_ids2[2, :], id2token, PAD)),
        ' '.join(target_tokens[2]),
        # ================================================
        # ================================================
        target_len)
    return out_string


data = read_pickles('Corpus/SilokCorpus/silok_15_10_im/test')
token2id = read_vocab('transformer/vocab/ch05im_kr05.vocab.87069')
id2token = dict_swap(token2id)
dataset = DataSet(100, 100, token2id, token2id)
dataset.make(data, train_phase=False)
print(get_running_configure(dataset, token2id, id2token))


