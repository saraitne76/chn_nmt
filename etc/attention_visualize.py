from models.seq2seq_attention import *
from models.bilstm_crf import *


# matplotlib_fonts()

mpl.rcParams['axes.unicode_minus'] = False
os.environ['CUDA_VISIBLE_DEVICES'] = '0'

model_nmt = 'Medium/NMT_IM/NMT_IM_CH05_KR05'
model_ner = 'NER/NER_IM_CH05_2L'

# source[91717]: 以李周鎭爲平安監司,  元景淳爲副校理,  尹敬周爲正言 。
# source[50667]: 分遣暗行御史李允明、金夢臣、李宇謙等,  廉察諸道 。
# source[27130]: 御熙政堂,  試文臣製述 。
# ch = '以金爔爲咸鏡南道節度使趙岐爲慶尙左道兵馬節度使崔朝岳爲公忠道兵馬節度使'
# ch = '以李周鎭爲平安監司, 元景淳爲副校理, 尹敬周爲正言。'
ch = '御熙政堂, 試文臣製述。'
reg_dic = read_dic('Corpus/compatibility2unified.txt', 2)
ch = chinese_regularize(ch, reg_dic)

graph_ner = tf.Graph()
with graph_ner.as_default():
    recognizer = BiLstmCrf(model_ner, args=None)
sess_ner = tf.Session(config=get_tf_config(), graph=graph_ner)
recognizer.init_model(sess=sess_ner, ckpt='step-50000')

graph_nmt = tf.Graph()
with graph_nmt.as_default():
    translator = Seq2SeqAttention(name=model_nmt, args=None)
    translator.build_model(train_phase=False,
                           num_gpu=1,
                           batch_size=1,
                           enc_max_len=len(ch),
                           dec_max_len=200,
                           gradients_clip=None,
                           beam_width=10,
                           alignment_history=True)
sess_nmt = tf.Session(config=get_tf_config(), graph=graph_nmt)
translator.init_model(sess_nmt, ckpt='step-300000')

kr, att_wgt = translator.run(sess_nmt, ch, cleaning=False)

print(len(kr), kr)
print(len(list(ch)), list(ch))
print(att_wgt.shape)

# input_tags - word representation of input sequence, use None to skip
# output_tags - word representation of output sequence, use None to skip
# i - index of input element in batch
ch_entities = recognizer.runs(sess_ner, [ch], 1)
ch_entities = ch_entities[0]
print(entity_lookup(att_wgt, ch_entities, kr))
# plot_attention(att_wgt, ch, kr)
plot_attention(att_wgt.T, ch, kr)
plt.show()

sess_ner.close()
sess_nmt.close()
