from transformer.transformer_wrapper import *
from pprint import pprint

os.chdir('/home/saraitne/PycharmProjects/CHN_NMT')

os.environ['CUDA_VISIBLE_DEVICES'] = '0'

model = 'Medium/Transformer_NoW2V/Transformer_NMT_IM_CH05_KR20'

params = model_params.BASE_PARAMS
params['batch_size'] = 1
token2id = read_vocab('transformer/vocab/ch05im_kr20.vocab.49997')


translator = TransformerWrapper(model, params, token2id, embedding=None, num_gpu=1, train_phase=False)
sess = tf.Session(config=get_tf_config())

# test_data = read_pickles('Corpus/SilokCorpus/silok_15_10_im/test')
# sample_data = test_data[5475]

sample_data = '以李周鎭爲平安監司, 元景淳爲副校理, 尹敬周爲正言。'
print(len(sample_data))
# print(sample_data.print())

out = translator.run_test(sess, sample_data, ckpt='step-300000')
# pprint(out)
# pprint(state)

# inputs = sample_data.ch_tok
translated = ids2seq(out['outputs'][0], dict_swap(token2id), EOS)
print(translated)
print(len(translated))
# print(sample_data.ch_tok)
# print(len(sample_data.ch_tok))

att = np.mean(out['att_w'][0], axis=0)
pprint(att)
print(np.shape(att))
# print('input_len', len(inputs))
# print(inputs)
print('output_len', len(translated))
print(translated)
# print('truth_len', len(sample_data.kr_tok))
# print(sample_data.kr_tok)
plot_attention(att[:len(translated), :len(sample_data)].T, None, None)
plt.show()

sess.close()
