from utils import *     # noqa
import time             # noqa


def main():
    num_examples = 1000
    max_seq_len = 300
    result_file = 'ner_dictionary_method_train_300.txt'
    if os.path.exists(result_file):
        raise ValueError('%s is existing' % result_file)

    data = read_pickles('Corpus/NERCorpus/silok_15_10_im/test')
    test_data = []
    for d in data:
        if d.seq_len <= max_seq_len:
            test_data.append(d)
    n_data = len(test_data)

    gaz = read_lexicon('Corpus/NERCorpus/gazetteer_train_15_10.txt', 5)
    gaz.sort(key=lambda x: len(x[0]), reverse=True)

    true_ne = []
    pred_ne = []

    s = time.time()
    for i, d in enumerate(test_data):
        entities = search_entity_by_dictionary(d.chinese, gaz)
        true_ne.append(d.entities)
        pred_ne.append(entities)
        print('\r%d/%d' % (i, n_data), end='')

    # print(true_ne)
    # print(pred_ne)

    f1_et, prcs_et, rcll_et = entity_form(true_ne, pred_ne)
    f1_sf, prcs_sf, rcll_sf = surface_form(true_ne, pred_ne)

    print()

    with open(result_file, mode='w', encoding='utf-8') as f:
        print_write("""
                    Testing Dictionary Method - Num_data: %d
                    entity form[f1: %0.3f, precision: %0.3f, recall: %0.3f]
                    surface form[f1: %0.3f, precision: %0.3f, recall: %0.3f]
                    time: %s
                    """
                    % (n_data, f1_et, prcs_et, rcll_et, f1_sf, prcs_sf, rcll_sf,
                       time.strftime('%Hh %Mm %Ss', time.gmtime(time.time() - s))), f)

        for i in range(num_examples):
            n = random.randint(0, n_data)
            s = """
                  source[%d]: %s
              prediction[%d]: %s
            ground truth[%d]: %s
                """ % (n, ' '.join(test_data[n].chinese), n, pred_ne[n], n, true_ne[n])
            print_write(s, f)
    return


if __name__ == '__main__':
    main()
