# Chinese-to-Korean NMT with Named Entity Recognition

## Motivation
![motivation](./etc/poster_pictures/picture1.png)

## Introduction
![introduction](./etc/poster_pictures/picture2.png)

## Method
### Step 1.
![method1](./etc/poster_pictures/picture3.png)
### Step 2.
![method2](./etc/poster_pictures/picture4.png)
### Step 3.
![method3](./etc/poster_pictures/picture5.png)
### Step 4.
![method4](./etc/poster_pictures/picture6.png)

## Experiments
![experiment](./etc/poster_pictures/picture7.png)