import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

from utils import *                 # noqa
from gensim.models import Word2Vec  # noqa
import argparse                     # noqa
import tensorflow as tf                                         # noqa
from tensorflow.contrib.tensorboard.plugins import projector    # noqa


def visualization(word2vec_model: str):
    save_dir = word2vec_model.split('.')[0]
    os.mkdir(save_dir)
    model = Word2Vec.load(word2vec_model)
    vectors = model.wv.vectors
    idx2word = model.wv.index2word
    with open(os.path.join(save_dir, 'metadata.tsv'), 'w', encoding='utf-8') as f:
        for word in idx2word:
            f.write(word + '\n')
    sess = tf.InteractiveSession()
    with tf.device("/cpu:0"):
        embedding = tf.Variable(vectors, trainable=False, name='embedding')
    tf.global_variables_initializer().run()
    saver = tf.train.Saver()
    writer = tf.summary.FileWriter(save_dir, sess.graph)

    config = projector.ProjectorConfig()
    embed = config.embeddings.add()
    embed.tensor_name = 'embedding'
    embed.metadata_path = 'metadata.tsv'

    projector.visualize_embeddings(writer, config)

    saver.save(sess, os.path.join(save_dir, 'model.ckpt'))
    sess.close()


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-w', '--word2vec', type=str,
                        help='word2vec model path')
    args = parser.parse_args()

    visualization(args.word2vec)
    print('done!')


if __name__ == '__main__':
    main()
