import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(os.path.dirname(__file__)))))

from Corpus.NERCorpus.data_utils import *       # noqa
import argparse                                 # noqa
import random                                   # noqa


def make(xmls: List[str], target_dir: str, norm_dic_path: str, only_chinese: bool, n_shard: int):
    os.makedirs(target_dir)
    norm_dic = read_dic(norm_dic_path, 2)

    level5s = []
    n_xml = len(xmls)
    for i, xml in enumerate(xmls):
        doc = ElementTree.parse(xml)
        root = doc.getroot()
        level5s.extend(get_child_elements(root, 'level5'))
        print('\rread xml files ... %d/%d   ' % (i, n_xml), end='')
    print()
    n_level5 = len(level5s)

    data = []
    cnt_para = 0
    for i, level5 in enumerate(level5s):
        paras = get_child_elements(level5, 'paragraph')
        level5_id = level5.attrib['id']
        for para in paras:
            cnt_para += 1
            ch, y, e = para2data(para, level5_id, norm_dic, only_chinese)
            if ch is not None:
                data.append(ParaData(ch, level5_id, y, len(ch), e))

        print('\r%s ... %d/%d   ' % (target_dir, i, n_level5), end='')
    print()

    n_data = len(data)

    nn = n_data // n_shard
    for i in range(n_shard-1):
        pickle_store(data[i*nn:(i+1)*nn], os.path.join(target_dir, 'data_%d.p' % i))
    pickle_store(data[(n_shard-1)*nn:], os.path.join(target_dir, 'data_%d.p' % (n_shard-1)))

    read_data = read_pickles(target_dir)
    n_read_data = len(read_data)
    assert n_data == n_read_data        # check the number of made data and stored data is same.

    with open(os.path.join(target_dir, 'info.txt'), 'w') as f:
        f.write('==================================Data Information==================================\n')
        f.write('Only Chinese: %s\n' % only_chinese)
        f.write('Normalization Dictionary: %s\n' % norm_dic_path)
        f.write('The Number of level5: %s\n' % n_level5)
        f.write('The number of paragraphs: %s\n' % cnt_para)
        f.write('The Number of data: %s\n' % n_data)
        examples = random.sample(data, 10)
        f.write('Examples\n')
        for example in examples:
            f.write('%s' % example.print())
        f.write('====================================================================================\n')
    return


def main():
    xml_dir = '/home/saraitne/공공데이터_조선왕조실록/sillok'
    xmls = os.listdir(xml_dir)
    xmls.remove('history.dtd')
    # remove 악보
    xmls.remove('2nd_wda_300.xml')
    # remove 칠정산내외편
    xmls.remove('2nd_wda_500.xml')
    xmls.sort()

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--train_kidx', type=str, default='1-20',
                        help='train dataset k00 index (1-25)')
    parser.add_argument('--dir', type=str, default='split_x',
                        help='Data destination directory')
    parser.add_argument('--only_chinese', type=int,
                        help='True(1), False(0)')
    parser.add_argument('--norm_dic', type=str, default='../compatibility2unified.txt',
                        help='Normalization dictionary path')
    parser.add_argument('--shard', type=int, default=10,
                        help='The number of shard')

    args = parser.parse_args()

    if args.only_chinese == 0:
        only_chinese = False
    elif args.only_chinese == 1:
        only_chinese = True
    else:
        raise ValueError('only chinese argument 0 or 1 but input: %d' % args.only_chinese)

    train_dir = os.path.join(args.dir, 'train')
    test_dir = os.path.join(args.dir, 'test')

    s = int(args.train_kidx.split('-')[0])
    e = int(args.train_kidx.split('-')[1])
    train_kidx = list(range(s, e+1))

    train_xml = []
    test_xml = []
    for xml in xmls:
        kk = xml.split('_')[1]
        if CODE2KING[kk][0] in train_kidx:
            train_xml.append(os.path.join(xml_dir, xml))
        else:
            test_xml.append(os.path.join(xml_dir, xml))

    train_kings = []
    test_kings = []
    for kidx, king in CODE2KING.values():
        if kidx in train_kidx:
            train_kings.append(king)
        else:
            test_kings.append(king)
    train_kings.sort()
    test_kings.sort()

    os.makedirs(args.dir)
    split_info = open(os.path.join(args.dir, 'split_info.txt'), 'w', encoding='utf-8')
    split_info.write('====================Train====================\n')
    split_info.write('The Number of Train Kings: %d\n' % len(train_kings))
    for kk in train_kings:
        split_info.write(kk+'\n')
    split_info.write('=============================================\n')

    split_info.write('====================Test=====================\n')
    split_info.write('The Number of Test Kings: %d\n' % len(test_kings))
    for kk in test_kings:
        split_info.write(kk + '\n')
    split_info.write('=============================================\n')
    split_info.close()

    make(train_xml, train_dir, args.norm_dic, only_chinese, args.shard)
    make(test_xml, test_dir, args.norm_dic, only_chinese, args.shard)


if __name__ == '__main__':
    main()
