import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

from utils import *                             # noqa
import matplotlib.pyplot as plt                 # noqa
import argparse                                 # noqa


def seq_hist(data_path: str, bins: int):
    data = read_pickles(data_path)
    print('number of data: %d' % len(data))

    # get whole train data sequence length
    ch_seq_len = list(map(lambda x: x.seq_len, data))

    # sort (big value is latter)
    ch_seq_len.sort()

    # drop biggest 10%
    ch_seq_len = ch_seq_len[:int(0.9 * len(ch_seq_len))]

    plt.hist(ch_seq_len, bins, label=data_path)
    plt.legend(loc='upper right')
    plt.show()


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--data', type=str, default='split_X/train',
                        help='Pair data path')
    parser.add_argument('-b', '--bins', type=int, default=10,
                        help='The number of histogram bins')
    args = parser.parse_args()

    seq_hist(args.data, args.bins)


if __name__ == '__main__':
    main()
