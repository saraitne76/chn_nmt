import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

from Data.data_utils import *               # noqa
import argparse                             # noqa


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-d', '--dir', type=str, default='split_x/train',
                        help='Data Directory')
    parser.add_argument('-i', '--id', type=str,
                        help='level5 id')

    args = parser.parse_args()
    read_data = read_pickles(args.dir)

    specific_data = search_data_by_id(read_data, args.id)

    for d in specific_data:
        with open('data_test_print.txt', 'w', encoding='utf-8') as f:
            f.write('%s' % d.print())
