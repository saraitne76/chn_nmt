import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(os.path.dirname(__file__)))))

from xml.etree import ElementTree       # noqa
from utils import *                     # noqa
import numpy as np                      # noqa


space = re.compile(r'\s{2,}')
not_ch = re.compile(r'[^\u2E80-\u2EFF\u3400-\u4DBF\u4E00-\u9FBF\uF900-\uFAFF]')


def cnt_element(xml_file, element):
    doc = ElementTree.parse(xml_file)
    cnt = 0
    root = doc.getroot()
    for _ in root.iter(element):
        cnt += 1
    return cnt


def get_child_elements(root, element):
    elements = []
    for element in root.iter(element):
        elements.append(element)
    return elements


def para2data(para_elmt, level5_id, norm_dic: Dict, only_chinese=False):
    if only_chinese:
        def p(s):
            return chinese_regularize(not_ch.sub('', space.sub(' ', s)), norm_dic)
    else:
        def p(s):
            return chinese_regularize(space.sub(' ', s), norm_dic)

    quo = para_elmt.find('quotation')
    if quo is not None:
        para = quo
    else:
        para = para_elmt

    try:
        ch = p(para.text)
        if is_redundant(ch, 0):
            return None, None, None
        entities = []
        for child in para:
            if child.tag == 'index':
                ch, entities = _index(child, ch, p, entities, level5_id)
            elif child.tag == 'annotation':
                ch, entities = _annotation(child, ch, p, entities, level5_id)
            elif child.tag == 'proofreading':
                ch, entities = _proofreading(child, ch, p, entities, level5_id)
            elif child.tag == 'newChar':
                ch = _other(child, ch, p, level5_id)
            elif child.tag == 'postScript':
                pass
            else:
                raise ValueError('%s, %s' % (list(para), level5_id))

        y = np.zeros(len(ch), dtype=np.uint8)
        for entity in entities:
            y[entity[1]:entity[1]+len(entity[0])] = TYPE2IDX[entity[2]][1]
            y[entity[1]] = TYPE2IDX[entity[2]][0]
        return ch, y, entities

    except TypeError:
        return None, None, None


def _other(element, sentence, prepro, level5_id):
    if len(list(element)) != 0:
        raise ValueError('%s, %s, %s' % (element.tag, list(element), level5_id))
        # input('%s, %s, %s' % (element.tag, list(element), level5_id))
    t1 = prepro(element.text)
    t2 = prepro(element.tail)
    # ('entity', start_index, type)
    sentence = sentence + t1 + t2
    return sentence


def _index(element, sentence, prepro, entities, level5_id):
    t1 = prepro(element.text)
    for child in element:
        if child.tag == 'annotation' and child.attrib['type'] == '교감주':
            t1 = t1 + prepro(child.tail)
        elif child.tag == 'proofreading' or child.tag == 'newChar':
            t1 = t1 + prepro(child.text + child.tail)
        else:
            raise ValueError('%s, %s, %s' % (element.tag, list(element), level5_id))
    t2 = prepro(element.tail)
    # ('entity', start_index, type)
    entities.append((t1, len(sentence), element.attrib['type']))
    sentence = sentence + t1 + t2
    return sentence, entities


def _annotation(element, sentence, prepro, entities, level5_id):
    # annotation type is 교감주 or 번역주 -> skip
    if element.attrib['type'] == '교감주' or element.attrib['type'] == '번역주':
        sentence += prepro(element.tail)
        return sentence, entities
    elif element.attrib['type'] == '원주':
        if len(list(element)) != 1:
            raise ValueError('%s, %s, %s' % (element.tag, list(element), level5_id))
        note = element[0]
        sentence += prepro(note.text)
        for child in note:
            if child.tag == 'index':
                sentence, entities = _index(child, sentence, prepro, entities, level5_id)
            elif child.tag == 'annotation':
                sentence, entities = _annotation(child, sentence, prepro, entities, level5_id)
            elif child.tag == 'proofreading' or child.tag == 'quotation':
                sentence, entities = _proofreading(child, sentence, prepro, entities, level5_id)
            elif child.tag == 'newChar':
                sentence = _other(child, sentence, prepro, level5_id)
            else:
                raise ValueError('%s, %s, %s' % (element.tag, list(element), level5_id))
        sentence += prepro(element.tail)
        return sentence, entities
    else:
        raise ValueError('%s, %s, %s' % (element.tag, list(element), level5_id))


def _proofreading(element, sentence, prepro, entities, level5_id):
    sentence += prepro(element.text)
    for child in element:
        if child.tag == 'annotation':
            sentence, entities = _annotation(child, sentence, prepro, entities, level5_id)
        elif child.tag == 'index':
            sentence, entities = _index(child, sentence, prepro, entities, level5_id)
        elif child.tag == 'proofreading':
            sentence, entities = _proofreading(child, sentence, prepro, entities, level5_id)
        elif child.tag == 'postScript':
            sentence += prepro(child.tail)
        elif child.tag == 'newChar':
            sentence = _other(child, sentence, prepro, level5_id)
        else:
            raise ValueError('%s, %s, %s' % (element.tag, list(element), level5_id))
    sentence += prepro(element.tail)
    return sentence, entities


def is_redundant(sentence: str, check=0):
    """
    :param sentence: A sentence
    :param check: hanja - 0  hangeul - 1
    :return: sentence is redundant return True
                    not redundant return False
    """
    for c in sentence:
        if unicode_classify(c) == check:    # 0 hanja, 1 hangeul
            return False
    return True


def get_level5_by_id(level5s, level5_id):
    for level5 in level5s:
        if level5.attrib['id'] == level5_id:
            return level5
    return None


def search_data_by_id(data: List[ParaData], level5_id):
    search = []
    n_data = len(data)
    for i, d in enumerate(data):
        if d.level5_id == level5_id:
            search.append(d)
        print('\rsearch...%d/%d' % (i, n_data), end='')
    print()
    return search
