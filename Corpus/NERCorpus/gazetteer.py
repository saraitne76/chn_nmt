import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(os.path.dirname(__file__)))))

from Corpus.NERCorpus.data_utils import *                # noqa
import argparse                                         # noqa


def make_gazetteer(xml_files, txt_file_name):
    gaz = []
    for xml_file in xml_files:
        doc = ElementTree.parse(xml_file)
        level2 = doc.getroot()

        _indices = get_child_elements(level2, 'index')
        for _index in _indices:
            gaz.append((_index.text, _index.attrib['type']))

    _, types = zip(*gaz)
    types_class = list(set(types))
    types_num = []
    types_ratio = []
    for t in types_class:
        types_num.append((t, types.count(t)))
        types_ratio.append((t, types.count(t)/len(types)))

    with open(txt_file_name, 'wt', encoding='utf-8') as f:
        f.write('The number of entity appear: %d\n' % len(gaz))
        f.write('The number of entity classes: %d\n' % len(set(gaz)))
        f.write('Entity Types: %s\n' % types_class)
        f.write('Entity Types Appear Num: %s\n' % types_num)
        f.write('Entity Types Ratio: %s\n' % types_ratio)

        for g in set(gaz):
            f.write('%s\t%s\n' % (g[0], g[1]))
    return


def main():
    xml_dir = '/home/saraitne/공공데이터_조선왕조실록/sillok'
    xmls = os.listdir(xml_dir)
    xmls.remove('history.dtd')
    # remove 악보
    xmls.remove('2nd_wda_300.xml')
    # remove 칠정산내외편
    xmls.remove('2nd_wda_500.xml')
    xmls.sort()

    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--train_kidx', type=str, default='1-20',
                        help='train dataset k00 index (1-25)')

    args = parser.parse_args()

    s = int(args.train_kidx.split('-')[0])
    e = int(args.train_kidx.split('-')[1])
    train_kidx = list(range(s, e+1))

    train_xml = []
    test_xml = []
    for xml in xmls:
        kk = xml.split('_')[1]
        if CODE2KING[kk][0] in train_kidx:
            train_xml.append(os.path.join(xml_dir, xml))
        else:
            test_xml.append(os.path.join(xml_dir, xml))

    make_gazetteer(train_xml, 'gazetteer_train_15_10.txt')
    make_gazetteer(test_xml, 'gazetteer_test_15_10.txt')


if __name__ == '__main__':
    main()
