import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))


dest = 'SilokCorpus/silok_15_10_im/word2vec'
data = 'SilokCorpus/silok_15_10_im/train SilokCorpus/silok_15_10_im/test'


lang = 0
window = 4
min_feq = 5
dim = [256, 512]
proc = 8
iter_num = 100

for d in dim:
    os.system('python word2vec.py --dest %s --data %s -l %s -w %s -f %s -d %s -p %s -i %s'
              % (dest, data, lang, window, min_feq, d, proc, iter_num))

lang = 1
window = 8
min_feq = [5, 30]
dim = [256, 512]
proc = 8
iter_num = 100

for d in dim:
    for f in min_feq:
        os.system('python word2vec.py --dest %s --data %s -l %s -w %s -f %s -d %s -p %s -i %s'
                  % (dest, data, lang, window, f, d, proc, iter_num))
