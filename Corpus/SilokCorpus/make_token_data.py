import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(os.path.dirname(__file__)))))

from utils import *                                                                     # noqa
import argparse                                                                         # noqa
from Corpus.SilokCorpus.chinese_preprocess import preprocess_chinese_from_txt           # noqa
from Corpus.SilokCorpus.korean_preprocess import preprocess_korean_from_txt             # noqa
from multiprocessing import Process                                                     # noqa
from konlpy.tag import Mecab                                                            # noqa


class Args:
    def __init__(self,
                 save_dir: str,
                 only_hanzi: bool,
                 mecab_dic_path: str,
                 reg_dic_path: str):
        self.save_dir = save_dir
        self.only_hanzi = only_hanzi

        self.tagger = Mecab(mecab_dic_path)
        self.mecab_dic_path = mecab_dic_path

        self.reg_dic_path = reg_dic_path
        self.reg_dic = read_dic(self.reg_dic_path, start_line=2)

    def print(self):
        s = """
        =====================Token Data Configure=====================
        Save Directory: %s
        Only Hanzi: %s
        Mecab Tagger: %s
        Regularize Dictionary: %s
        ==============================================================
        \n\r""" % (self.save_dir,
                   self.only_hanzi,
                   self.mecab_dic_path,
                   self.reg_dic_path)
        return s


def data_write(args: Args,
               pickle_name: str,
               files: List[str]):
    data_list = list()
    n_files = len(files)
    for i, txt in enumerate(files):
        ch, ch_tokens = preprocess_chinese_from_txt(txt, args.reg_dic, only_hanzi=args.only_hanzi)
        kr, kr_tokens = preprocess_korean_from_txt(txt, args.tagger)
        d = PairData(ch_sent=ch,
                     ch_tok=ch_tokens,
                     kr_sent=kr,
                     kr_tok=kr_tokens)
        data_list.append(d)
        print('\rProcessing... %0.2f%%' % ((i / n_files) * 100), end='')

    pickle_store(data_list, os.path.join(args.save_dir, pickle_name))
    print('\n%s is stored\n' % pickle_name, end='')


def processes_run(kings: List[str],
                  n_process: int,
                  save_dir: str,
                  only_hanzi: bool,
                  mecab_dic: str,
                  reg_dic: str):
    files = get_txt_paths(SILOK_PATH, kings)
    n_file = len(files)

    dw_args = Args(save_dir=save_dir,
                   only_hanzi=only_hanzi,
                   mecab_dic_path=mecab_dic,
                   reg_dic_path=reg_dic)

    os.makedirs(dw_args.save_dir)
    config = open(os.path.join(save_dir, CONFIG_TXT), 'w', encoding='utf-8')
    config.write(dw_args.print())
    config.write('The Number of Kings: %d\n' % len(kings))
    for k in kings:
        config.write(k + '\n')
    config.write('The Number of Articles: %d\n' % n_file)
    config.close()

    del dw_args

    shard = n_file // n_process
    assigns = []
    for n in range(n_process-1):
        assigns.append(files[n*shard:(n+1)*shard])
    assigns.append(files[(n_process-1)*shard:])

    assert len(assigns) == n_process
    restoration = []
    for a in assigns:
        restoration += a

    assert len(restoration) == len(files), 'len(restoration) = %d, len(files) = %d' % (len(restoration), len(files))
    for a, b in zip(restoration, files):
        if a != b:
            raise ValueError('%s != %s' % (a, b))

    processes = []
    for i in range(n_process):
        with open(os.path.join(save_dir, CONFIG_TXT), 'a', encoding='utf-8') as config:
            pickle_name = 'data_%02d.p' % i
            assigned = assigns[i]
            config.write('Pickle %s has %d data\n' % (pickle_name, len(assigned)))
        dw_args = Args(save_dir=save_dir,
                       only_hanzi=only_hanzi,
                       mecab_dic_path=mecab_dic,
                       reg_dic_path=reg_dic)
        p = Process(target=data_write,
                    args=(dw_args, pickle_name, assigned))
        processes.append(p)
        p.start()

    for i, p in enumerate(processes):
        p.join()

    stored = read_pickles(save_dir)
    with open(os.path.join(save_dir, CONFIG_TXT), 'a', encoding='utf-8') as config:
        config.write('======================================================\n')
        config.write('The Number of Total Stored Data: %d\n' % len(stored))
        examples = random.sample(stored, 5)
        for example in examples:
            config.write(example.print())
    return


def main():
    kk = [(0, 5), (6, 10), (11, 15), (16, 20), (21, 25), (26, 30)]
    for a, b in kk:
        print(kings_total[a:b])
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--train_kidx', type=str, default='1-20',
                        help='train dataset k00 index (1-25)')
    parser.add_argument('-p', '--processes', type=int, default=8,
                        help='The number of processes')
    parser.add_argument('--dir', type=str, default='split',
                        help='destination directory - split_*')
    parser.add_argument('-o', '--only_hanzi', type=int, default=1,
                        help='1 -> only hanzi, 0 -> include all sentence mark')
    parser.add_argument('--mecab_dic', type=str, default='/usr/local/lib/mecab/dic/mecab-ko-dic',
                        help='Mecab tagger dictionary path')
    parser.add_argument('--reg_dic', type=str, default='../compatibility2unified.txt',
                        help='regularize dictionary path')
    args = parser.parse_args()

    if args.only_hanzi == 1:
        only_hanzi = True
    elif args.only_hanzi == 0:
        only_hanzi = False
    else:
        raise ValueError('Argument rm_mark: %s' % args.rm_mark)

    # print(args.train_kidx)
    s = int(args.train_kidx.split('-')[0])
    e = int(args.train_kidx.split('-')[1])
    train_kidx = list(map(lambda x: 'k%02d' % x, list(range(s, e + 1))))

    kings_total_temp = kings_total[:]
    kings_train = list()
    for king in kings_total_temp:
        for kidx in train_kidx:
            if kidx in king:
                kings_train.append(king)

    for king in kings_train:
        kings_total_temp.remove(king)
    kings_test = kings_total_temp

    kings_train.sort()
    kings_test.sort()

    # print()
    # print(len(kings_total), kings_total)
    # print(len(kings_train), kings_train)
    # print(len(kings_test), kings_test)

    os.makedirs(args.dir)
    split_info = open(os.path.join(args.dir, 'split_info.txt'), 'w', encoding='utf-8')
    split_info.write('====================Train====================\n')
    split_info.write('The Number of Train Kings: %d\n' % len(kings_train))
    for kk in kings_train:
        split_info.write(kk + '\n')
    split_info.write('=============================================\n')

    split_info.write('====================Test=====================\n')
    split_info.write('The Number of Test Kings: %d\n' % len(kings_test))
    for kk in kings_test:
        split_info.write(kk + '\n')
    split_info.write('=============================================\n')
    split_info.close()

    if len(kings_train) != 0:
        processes_run(kings=kings_train,
                      n_process=args.processes,
                      save_dir=os.path.join(args.dir, 'train'),
                      only_hanzi=only_hanzi,
                      mecab_dic=args.mecab_dic,
                      reg_dic=args.reg_dic)

    if len(kings_test) != 0:
        processes_run(kings=kings_test,
                      n_process=args.processes,
                      save_dir=os.path.join(args.dir, 'test'),
                      only_hanzi=only_hanzi,
                      mecab_dic=args.mecab_dic,
                      reg_dic=args.reg_dic)


if __name__ == '__main__':
    main()
