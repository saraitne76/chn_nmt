import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(os.path.dirname(__file__)))))

from utils import *                             # noqa


def find_end(text: str):
    end_mark = ['【태백산사고본】', '【정족산사고본】', '【국편영인본】', '【원본】']
    e = []
    for mark in end_mark:
        t = text.find(mark)
        if t > 0:
            e.append(t)
    return min(e)


def get_ch(text_file: str):
    t = text_read(text_file)
    e = find_end(t)
    s = t.find('\n원문\n')+3
    t = t[s:e]
    idx = 0
    for i, c in enumerate(t):
        if unicode_classify(c) == 0:
            idx = i
            break
    return t[idx:]


def preprocess_chinese_from_txt(txt_path: str, reg_dic: Dict, only_hanzi: bool):
    """
    :param txt_path: text file path
    :param reg_dic: regularization dictionary
    :param only_hanzi: if True, not hanzi character will be removed
    :return: cleaned chinese sentence, chinese tokens, entities
    """

    # step 1. get Chinese sentence from a text file and remove redundant line
    ch = ' '.join(remove_redundant_line(get_ch(txt_path).split('\n'), check=0))

    # step 2. remove day name (육십갑자)
    if '/' in ch:
        ch = ch[ch.index('/') + 1:]

    # step 3. regularize chinese sentence (compatibility character -> unified character)
    ch = chinese_regularize(ch, reg_dic)

    # step 3-1. clean up not chinese character e.g. 문장부호
    if only_hanzi:
        ch = clean_up_ch(ch)

    # step 4. tokenize Chinese sentence character-by-character
    tokens = list(ch)

    return ch, tokens


def my_test():
    reg_dic = read_dic('../compatibility2unified.txt', start_line=2)

    import random
    txts = random.sample(get_txt_paths(SILOK_PATH, kings_total), 100)

    for txt in txts:
        ch1, tokens1 = preprocess_chinese_from_txt(txt, reg_dic, only_hanzi=True)
        ch2, tokens2 = preprocess_chinese_from_txt(txt, reg_dic, only_hanzi=False)
        print('===========================================================================')
        print(ch1)
        print(tokens1)
        print(ch2)
        print(tokens2)
        print('===========================================================================')


if __name__ == '__main__':
    my_test()
