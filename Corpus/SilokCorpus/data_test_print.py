import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(os.path.dirname(__file__)))))

from utils import *                     # noqa
import random                           # noqa
import time                             # noqa

s = time.time()
data_list = read_pickles('total_basic/train')
print('number of data: %d' % len(data_list))
print('read time: %f' % (time.time()-s))

dd = random.sample(data_list, 10)

for d in dd:
    print(d.print())
