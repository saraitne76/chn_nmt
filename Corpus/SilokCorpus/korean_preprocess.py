import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(os.path.dirname(__file__)))))

from utils import *                             # noqa
from konlpy.tag import Mecab                    # noqa


rm_redundant_from_kr = re.compile(r'\(\w+\)|\d+\)|〔\w+〕|\[\w+\]')


def get_kr(text_file: str):
    t = text_read(text_file)
    # juseok_idx = t.find('[註')
    # if juseok_idx == -1:
    #     t = t[t.find('\n국역\n')+3:t.find('\n원문\n')]
    # else:
    #     t = t[t.find('\n국역\n')+3:min(t.find('\n원문\n'), juseok_idx)]
    t = t[t.find('\n국역\n') + 3:t.find('\n원문\n')]
    idx = 0
    for i, c in enumerate(t):
        if unicode_classify(c) == 1 or c.isdigit():
            idx = i
            break
    return t[idx:]


def rm_juseok_from_kr(kr: str):
    if '[註' in kr:
        return kr[:kr.find('[註')]
    else:
        return kr


def preprocess_korean_from_txt(txt_path: str, tagger):
    # step 1. get Korean sentence from a text file and remove redundant line
    kr = ' '.join(remove_redundant_line(get_kr(txt_path).split('\n'), check=1))

    # step 2. clean up Korean sentence - remove (製述), 031), [註 006]
    kr = rm_redundant_from_kr.sub('', rm_juseok_from_kr(kr))

    # step 3. tokenize using updated mecab tagger (updated by SILOK entity dictionary)
    # TODO remove ('SY', '기타 기호') tokens
    morp = tagger.morphs(kr)

    # step 4. return Korean sentence & tokenized
    return kr, morp


def my_test():
    tagger = Mecab('/usr/local/lib/mecab/dic/mecab-ko-dic-custom')

    import random
    txts = random.sample(get_txt_paths(SILOK_PATH, kings_total), 100)

    for txt in txts:
        kr, tokens = preprocess_korean_from_txt(txt, tagger)
        print('===========================================================================')
        print(kr)
        print(tokens)
        print('===========================================================================')

    kk = '/home/saraitne/chosun-utf8/k03_Taejong/book019/kca_11001003_001.txt'
    kr, tokens = preprocess_korean_from_txt(kk, tagger)
    print('===========================================================================')
    print(kr)
    print(tokens)
    print('===========================================================================')

    # kk = '/home/saraitne/chosun-utf8/k15_Gwanghaegun_jungcho/book148/koa_11201009_002.txt'
    # kr, tokens, entities = preprocess_korean_from_txt(kk, tagger, reg_dic)
    # print('===========================================================================')
    # print(kr)
    # print(tokens)
    # print(entities)
    # print('===========================================================================')


if __name__ == '__main__':
    my_test()
