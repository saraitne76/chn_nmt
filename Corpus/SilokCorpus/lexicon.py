import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(os.path.dirname(__file__)))))

from Corpus.SilokCorpus.korean_preprocess import get_kr
from utils import *                                             # noqa
from hgtk.checker import has_batchim                            # noqa


ne_from_kr = re.compile(r'\([\u2E80-\u2EFF\u3400-\u4DBF\u4E00-\u9FBF\uF900-\uFAFF]{2,}\)')
ne2_from_kr = re.compile(r'\[[\u2E80-\u2EFF\u3400-\u4DBF\u4E00-\u9FBF\uF900-\uFAFF]{2,}\]')
digit = re.compile(r'\d')
max_entity_len = 6


def pronounce_from_dic(ne: str, p: Dict, pi: Dict):
    try:
        h = pi[ne[0]]
    except KeyError:
        h = '?'
    for i, c in enumerate(ne[1:]):
        try:
            h = h + p[c]
        except KeyError:
            h = h + '?'
    return h


def pronounce_from_kr(kr: str, entity: str, s_idx: int):
    len_entity = len(entity)
    kr_pron = str()
    kr_cnt = 0
    backward = 1
    while kr_cnt < len_entity:
        idx = s_idx - backward
        # assert idx > -1
        if idx < 0:
            # raise IndexError('%s, entity: %s, start_index: %d' % (kr, entity, s_idx))
            return entity, UNK, s_idx
        c = kr[idx]
        if unicode_classify(c) == 1 or c.isdigit():        # if Korean character or Digit
            kr_pron = c + kr_pron
            kr_cnt += 1
        elif c == ' ':
            kr_pron = c + kr_pron
        backward += 1
    assert len_entity == kr_cnt
    return entity, kr_pron, s_idx


def entity_split_by_space(entity: str, kr_pron: str, idx: int):
    """
    split 2 words in Korean but 1 words in Chinese
    e.g. 포도 대장(捕盜大將) -> 포도(捕盜), 대장(大將)
    :param entity: chinese entity word
    :param kr_pron: korean pronounce correspond with entity
    :param idx: chinese entity word start index from Korean sentence
    :return: splited
    """
    if ' ' in kr_pron:
        out = list()
        i = 0
        split = kr_pron.split(' ')
        for k in split:
            len_k = len(k)
            if len_k > 1:
                out.append((entity[i:i+len_k], k, idx+i))
            i += len_k
        return out
    else:
        return [(entity, kr_pron, idx)]


def rm_digit_entity(entities: List):
    """
    [('四更', '4경'), ('流星', '유성'), ('角星', '각성'), ('四方', '사방')]
    -> [('流星', '유성'), ('角星', '각성'), ('四方', '사방')]
    """
    o = list()
    for item in entities:
        if digit.search(item[1]) is None:
            o.append(item)
    return o


def extract_entities_from_dataset_sub(kr: str, p: Dict, pi: Dict):
    """
    extract parenthesized[]
    extract parenthesized[]
    extract parenthesized[]
    Extract named entities which are parenthesized[] with Korean pronounce from Korean sentences.
    """
    # Find entities in Korean sentences
    # which Chinese words are parenthesized[] and composed of two or more Chinese character
    entities = [(m.group().strip('[]'), m.start()+1) for m in ne2_from_kr.finditer(kr)]

    # get entity pronounce from Chinese character pronounce dictionary
    entities_with_kr = list(map(lambda x: (x[0], pronounce_from_dic(x[0], p, pi), x[1]), entities))
    return entities_with_kr


def extract_entities_from_dataset(kr: str):
    """
    Extract named entities which are parenthesized() with Korean pronounce from Korean sentences.
    :param kr: Korean sentences from text files including word written by Korean and Chinese together.
    :return: entities with Korean pronounce
    """
    # step 1. Find entities in Korean sentences
    # which Chinese words are parenthesized() and composed of two or more Chinese character
    entities_with_idx = [(m.group().strip('()'), m.start()+1) for m in ne_from_kr.finditer(kr)]
    # 문신(文臣)의 제술(製述)은 장례원 사의(掌隷院司議) 표연말(表沿沫)이 으뜸을 차지하니, 명하여 한 자급을 더하게 하였다.
    # entity: (文臣), start_index: 2
    # [('文臣', 2), ('製述', 10), ('掌隷院司議', 22), ('表沿沫', 33)]

    # step 2. get entity pronounce from Korean sentence e.g. 문신(文臣)
    # [('春塘臺', '춘당대', 4), ('隨駕', '수가', 17), ('軍校', '군교', 25), ('試射', '시사', 34)]
    entities_with_kr = list(map(lambda x: pronounce_from_kr(kr, x[0], x[1]), entities_with_idx))

    # step 3. remove digit entity e.g. ('四更', '4경') will be removed
    entities_with_kr = rm_digit_entity(entities_with_kr)

    # step4. split 2 words in Korean but 1 words in Chinese
    # e.g. 포도 대장(捕盜大將) -> 포도(捕盜),  대장(大將)
    entities = list()
    for e, k, i in entities_with_kr:
        split = entity_split_by_space(e, k, i)
        entities.extend(split)

    return entities


def main():
    reg_dic = read_dic('../compatibility2unified.txt', start_line=2)
    txts = get_txt_paths(SILOK_PATH, kings_total)

    n_files = len(txts)
    lexicon = list()
    for i, txt in enumerate(txts):
        # step 1. Get Korean sentence from a text file and remove redundant line
        kr = ''.join(remove_redundant_line(get_kr(txt).split('\n'), check=1))

        # step 2. Extract Chinese entities from Korean sentence
        # [('春塘臺', '춘당대', 4), ...]
        entities = extract_entities_from_dataset(kr)

        # step 3. Chinese character regularize and remove entity index
        entities_ = list(map(lambda x: (chinese_regularize(x[0], reg_dic), x[1]), entities))

        # step 4. Drop too long entity and Unknown Korean pronounce
        entities = []
        for e in entities_:
            if len(e[0]) <= max_entity_len and e[1] != UNK:
                entities.append(e)

        lexicon.extend(entities)
        print('\rfiles...%d/%d' % (i, n_files), end='')
    print()

    # step 4. Remove duplicate entity
    lexicon = list(set(lexicon))

    # step 5. Sort by entity length
    lexicon.sort(key=lambda x: len(x[0]), reverse=True)

    # step 6. Store entity list in Project dictionary
    with open('lexicon_silok.txt', 'w', encoding='utf-8') as f:
        for c, k in lexicon:
            f.write('%s\t%s\n' % (c, k))

    # step 7. Update mecab pos tagger dictionary
    with open('nnp_silok.csv', 'w', encoding='utf-8') as f:
        for c, k in lexicon:
            if k != 'UNK':
                b = str(has_batchim(k[-1]))[0]
                s = '%s,,,,NNP,*,%s,%s,*,*,*,*,*\n' % (k, b, k)
                f.write(s)


if __name__ == '__main__':
    main()
