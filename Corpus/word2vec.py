import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

from utils import *                 # noqa
from gensim.models import Word2Vec  # noqa
import logging                      # noqa
import argparse                     # noqa


def word2vec(data_path: List[str], language, model_file, word_dim, window_size, word_min_freq, n_processes, n_iter):

    data_list = list()
    for d in data_path:
        if os.path.isdir(d):
            data_list.extend(read_pickles(d))
        else:
            data_list.extend(pickle_load(d))

    num_data = len(data_list)
    if num_data < 1:
        raise ValueError('the number of input data < 1')

    with open(model_file.split('.')[0] + '.txt', 'w') as f:
        f.write('Data Path: %s\n' % data_path)
        f.write('# data: %s\n' % num_data)

    print('number of data: %d' % num_data)
    if language == 0:
        data = list(map(lambda x: x.ch_tok, data_list))
    elif language == 1:
        data = list(map(lambda x: x.kr_tok, data_list))
    else:
        raise ValueError('target language, 0: Chinese, 1: Korean but %s' % language)

    # print data to check
    # tt = random.sample(data, 5)
    # for t in tt:
    #     print(t)

    model = Word2Vec(sentences=data,
                     size=word_dim,
                     window=window_size,
                     min_count=word_min_freq,
                     workers=n_processes,
                     sg=1,
                     iter=n_iter)
    model.save(model_file)


def main(folder: str,
         data_path: List[str],
         language: int,
         window: int,
         word_min_freq: int,
         embedding_dim: int,
         workers: int,
         iteration: int):

    if language == 0:
        lang = 'Chinese'
    elif language == 1:
        lang = 'Korean'
    else:
        raise ValueError('target language, 0: Chinese, 1: Korean but %s' % language)

    model_file = '%s/%s_d%03d_w%02d_mc%02d/embedding.w2v' % (folder, lang, embedding_dim, window, word_min_freq)
    print(model_file)

    os.makedirs(os.path.dirname(model_file), exist_ok=True)

    fomatter = logging.Formatter('%(asctime)s:  %(levelname)s : %(message)s')

    file_handler = logging.FileHandler('%s/%s_d%03d_w%02d_mc%02d/embedding.log' %
                                       (folder, lang, embedding_dim, window, word_min_freq))
    file_handler.setFormatter(fomatter)
    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(fomatter)

    logging.basicConfig(level=logging.INFO, handlers=(file_handler, stream_handler))

    word2vec(data_path=data_path,
             language=language,
             model_file=model_file,
             word_dim=embedding_dim,
             window_size=window,
             word_min_freq=word_min_freq,
             n_processes=workers,
             n_iter=iteration)

    logging.root.removeHandler(file_handler)
    logging.root.removeHandler(stream_handler)
    file_handler.close()
    stream_handler.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--dest', type=str, default='pairs/word2vec',
                        help='Destination directory')
    parser.add_argument('--data', type=str, nargs='+',
                        help='Pair data path')
    parser.add_argument('-l', '--language', type=int, default=0,
                        help='target language, Chinese: 0, Korean: 1')
    parser.add_argument('-w', '--window', type=int, default=4,
                        help='Maximum distance between the current and predicted word within a sentence.')
    parser.add_argument('-f', '--min_freq', type=int, default=5,
                        help='Ignores all words with total frequency lower than this.')
    parser.add_argument('-d', '--dimension', type=int, default=300,
                        help='Dimensionality of the word vectors.')
    parser.add_argument('-p', '--processes', type=int, default=8,
                        help='The number of processes')
    parser.add_argument('-i', '--iter', type=int, default=100,
                        help='Learning iteration')
    args = parser.parse_args()

    main(folder=args.dest,
         data_path=args.data,
         language=args.language,
         window=args.window,
         word_min_freq=args.min_freq,
         embedding_dim=args.dimension,
         workers=args.processes,
         iteration=args.iter)
