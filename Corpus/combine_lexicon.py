import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

from utils import *                                         # noqa
import random                                               # noqa

lexicon_crc = read_lexicon('CrcCorpus/lexicon_crc.txt')
lexicon_silok = read_lexicon('SilokCorpus/lexicon_silok.txt')

print(random.sample(lexicon_crc, 10))
print(len(lexicon_crc))
print(random.sample(lexicon_silok, 10))
print(len(lexicon_silok))

# Combine lexicon from CRC and from silok
lexicon = lexicon_silok + lexicon_crc
print(len(lexicon))

# Remove duplicate
lexicon = list(set(lexicon))
print(len(lexicon))

# Sort by entity length
lexicon.sort(key=lambda x: len(x[0]), reverse=True)

# Save
with open('lexicon.txt', 'w', encoding='utf-8') as f:
    for c, k in lexicon:
        f.write('%s\t%s\n' % (c, k))
