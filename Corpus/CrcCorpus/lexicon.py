import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(os.path.dirname(__file__)))))

from Corpus.CrcCorpus.korean_preprocess import *            # noqa
from hgtk.checker import has_batchim                        # noqa
from hgtk.checker import NotHangulException                 # noqa
import argparse                                             # noqa


def main(file_path, max_entity_len):
    reg_dic = read_dic('../../Dictionary/compatibility2unified.txt', start_line=2)

    kr_sents = read_xlsx(file_path, [3])

    n_sents = len(kr_sents)
    lexicon = list()
    for i, kr in enumerate(kr_sents):
        # step 1. Extract Chinese entities from Korean sentence
        # [('春塘臺', '춘당대', 4), ...]
        entities = extract_entities_from_dataset(kr)

        # step 2. Chinese character regularize and remove entity index
        entities_ = list(map(lambda x: (chinese_regularize(x[0], reg_dic), x[1]), entities))

        # step 3. Drop too long entity and Unknown Korean pronounce
        entities = []
        for e in entities_:
            if len(e[0]) <= max_entity_len and e[1] != UNK:
                entities.append(e)
        lexicon.extend(entities)
        print('\rSentences...%d/%d' % (i, n_sents), end='')
    print()

    # step 4. Remove duplicate entity
    lexicon = list(set(lexicon))

    # step 5. Sort by entity length
    lexicon.sort(key=lambda x: len(x[0]), reverse=True)

    # step 6. Store entity list in Project dictionary
    with open('lexicon_crc.txt', 'w', encoding='utf-8') as f:
        for c, k in lexicon:
            f.write('%s\t%s\n' % (c, k))

    # step 7. Update mecab pos tagger dictionary
    with open('nnp_crc.csv', 'w', encoding='utf-8') as f:
        for c, k in lexicon:
            if k != 'UNK':
                try:
                    b = str(has_batchim(k[-1]))[0]
                except NotHangulException:
                    raise NotHangulException('%s %s' % (c, k))
                s = '%s,,,,NNP,*,%s,%s,*,*,*,*,*\n' % (k, b, k)
                f.write(s)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-f', '--target_file', type=str, default='Languagemodel dataset.xlsx',
                        help='target xlsx file path')
    parser.add_argument('-m', '--entity_max_len', type=int, default=5,
                        help='entity max length')
    args = parser.parse_args()
    main(args.target_file, args.entity_max_len)
