import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(os.path.dirname(__file__)))))

from utils import *                                                                         # noqa
import argparse                                                                             # noqa
from Corpus.CrcCorpus.chinese_preprocess import preprocess_chinese_from_sentence            # noqa
from Corpus.CrcCorpus.korean_preprocess import preprocess_korean_from_sentence              # noqa
from konlpy.tag import Mecab, Kkma, Okt                                                     # noqa
import random                                                                               # noqa


class Args:
    def __init__(self,
                 save_dir: str,
                 chinese_rm_mark: bool,
                 tagger_name: str,
                 mecab_dic_path: str,
                 lexicon_path: str,
                 reg_dic_path: str):
        self.save_dir = save_dir
        self.chinese_rm_mark = chinese_rm_mark

        self.tagger_name = tagger_name
        if self.tagger_name == 'Mecab':
            self.tagger = Mecab(mecab_dic_path)
            self.mecab_dic_path = mecab_dic_path
        elif self.tagger_name == 'Kkma':
            self.tagger = Kkma()
            self.mecab_dic_path = None
        elif self.tagger_name == 'Okt':
            self.tagger = Okt()
            self.mecab_dic_path = None
        else:
            raise ValueError('Invaild tagger name')

        self.lexicon_path = lexicon_path
        # self.lexicon = list(read_dic(self.lexicon_path, start_line=0).keys())
        self.lexicon = list(map(lambda x: x[0], read_lexicon(self.lexicon_path)))
        self.lexicon.sort(key=lambda x: len(x), reverse=True)

        self.reg_dic_path = reg_dic_path
        self.reg_dic = read_dic(self.reg_dic_path, start_line=2)

    def print(self):
        s = """
        =====================Token Data Configure=====================
        Save Directory: %s
        Chinese Remove Punctuations: %s
        Konlpy Tagger: %s
        Mecab Tagger: %s
        Lexicon: %s
        The Number of Entity: %s
            first 5 examples: %s 
            end 5 examples: %s
            random 5 examples: %s
        Regularize Dictionary: %s
        ==============================================================
        \n\r""" % (self.save_dir,
                   self.chinese_rm_mark,
                   self.tagger_name,
                   self.mecab_dic_path,
                   self.lexicon_path,
                   len(self.lexicon),
                   self.lexicon[:5],
                   self.lexicon[-5:],
                   random.sample(self.lexicon, 5),
                   self.reg_dic_path)
        return s


def data_write(args: Args,
               pickle_name: str,
               data: List[Tuple]):
    """
    :param args: Write Arguments
    :param pickle_name: Save pickle name
    :param data: List of Tuple [Chinese Sentence, Korean Sentence]
    :return: None
    """
    data_list = list()
    n_data = len(data)
    for i, d in enumerate(data):
        ch, ch_tokens, _ = preprocess_chinese_from_sentence(d, args.lexicon, args.reg_dic,
                                                            rm_mark=args.chinese_rm_mark)
        kr, kr_tokens, _ = preprocess_korean_from_sentence(d[1], args.tagger, args.reg_dic)
        d = PairData(ch_sent=ch,
                     ch_tok=ch_tokens,
                     kr_sent=kr,
                     kr_tok=kr_tokens)
        data_list.append(d)
        print('\rProcessing... %0.2f%%' % ((i / n_data) * 100), end='')

    pickle_store(data_list, os.path.join(args.save_dir, pickle_name))
    print('\n%s is stored\n' % pickle_name, end='')


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-d', '--destination', type=str, default='split_x',
                        help='Destination folder path')
    parser.add_argument('-t', '--target_xlsx', type=str, default='Languagemodel dataset.xlsx',
                        help='Target Excel File')
    parser.add_argument('-r', '--ratio', type=str, default='7:3',
                        help='Train-Test split ratio e.g. 5:5')
    parser.add_argument('--tagger', type=str, default='Mecab',
                        help='tagger name')
    parser.add_argument('--mecab_dic', type=str, default='/usr/local/lib/mecab/dic/mecab-ko-dic',
                        help='Mecab tagger dictionary path')
    parser.add_argument('--lexicon', type=str, default='../lexicon.txt',
                        help='lexicon path')
    parser.add_argument('--reg_dic', type=str, default='../../Dictionary/compatibility2unified.txt',
                        help='regularize dictionary path')
    parser.add_argument('--rm_mark', type=int, default=1,
                        help='1 -> remove, 0 -> no remove')
    parser.add_argument('-s', '--seed', type=int, default=777,
                        help='random seed')
    args = parser.parse_args()

    if args.rm_mark == 1:
        rm_mark = True
    elif args.rm_mark == 0:
        rm_mark = False
    else:
        raise ValueError('Argument rm_mark: %s' % args.rm_mark)

    train_test_ratio = args.ratio
    assert len(train_test_ratio.split(':')) == 2
    test_ratio = float(train_test_ratio.split(':')[-1]) / 10
    # folder = 'train%s_test%s' % tuple(train_test_ratio.split(':'))

    folder = args.destination
    os.makedirs(folder)

    data_set = read_xlsx(args.target_xlsx, [2, 3])
    data_set.pop(0)

    train = list()
    test = list()
    random.seed(args.seed)
    for ch, kr in data_set:
        if random.random() > test_ratio:
            train.append((ch, kr))
        else:
            test.append((ch, kr))

    dw_args = Args(folder, rm_mark, args.tagger, args.mecab_dic, args.lexicon, args.reg_dic)
    with open(os.path.join(folder, CONFIG_TXT), 'w', encoding='utf-8') as f:
        f.write('Excel Data: %s\n' % args.target_xlsx)
        f.write('# Train Sentences: %s\n' % len(train))
        f.write('# Test Sentences: %s\n' % len(test))
        f.write(dw_args.print())

    data_write(dw_args, 'train.p', train)
    data_write(dw_args, 'test.p', test)


if __name__ == '__main__':
    main()
