import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(os.path.dirname(__file__)))))

from utils import *                             # noqa
import re                                       # noqa
from konlpy.tag import Mecab                    # noqa


ne_from_kr = re.compile(r'[\u2E80-\u2EFF\u3400-\u4DBF\u4E00-\u9FBF\uF900-\uFAFF]{2,}')
not_ne = re.compile(r'\[[\u2E80-\u2EFF\u3400-\u4DBF\u4E00-\u9FBF\uF900-\uFAFF]{2,}\]')
rm_redundant_from_kr = \
    re.compile(r'\([^)]+\)|\[[^]]+\]|[\u2E80-\u2EFF\u3400-\u4DBF\u4E00-\u9FBF\uF900-\uFAFF]')
# rm_redundant_from_kr = \
#      re.compile(r'\(\w+\)|〔\w+〕|\[\w+\]')
digit = re.compile(r'\d')


def pronounce_from_dic(ne: str, p: Dict, pi: Dict):
    try:
        h = pi[ne[0]]
    except KeyError:
        h = '?'
    for i, c in enumerate(ne[1:]):
        try:
            h = h + p[c]
        except KeyError:
            h = h + '?'
    return h


def pronounce_from_kr(kr: str, entity: str, s_idx: int):
    len_entity = len(entity)
    kr_pron = str()
    kr_cnt = 0
    backward = 1
    while kr_cnt < len_entity:
        idx = s_idx - backward
        # assert idx > -1
        if idx < 0:
            # raise IndexError('%s, entity: %s, start_index: %d' % (kr, entity, s_idx))
            return entity, UNK, s_idx
        c = kr[idx]
        if unicode_classify(c) == 1 or c.isdigit():        # if Korean character or Digit
            kr_pron = c + kr_pron
            kr_cnt += 1
        elif c == ' ':
            kr_pron = c + kr_pron
        backward += 1
    assert len_entity == kr_cnt
    return entity, kr_pron, s_idx


def rm_digit_entity(entities: List):
    """
    [('四更', '4경'), ('流星', '유성'), ('角星', '각성'), ('四方', '사방')]
    -> [('流星', '유성'), ('角星', '각성'), ('四方', '사방')]
    """
    o = list()
    for item in entities:
        if digit.search(item[1]) is None:
            o.append(item)
    return o


def entity_split_by_space(entity: str, kr_pron: str, idx: int):
    """
    split 2 words in Korean but 1 words in Chinese
    e.g. 포도 대장(捕盜大將) -> 포도(捕盜), 대장(大將)
    :param entity: chinese entity word
    :param kr_pron: korean pronounce correspond with entity
    :param idx: chinese entity word start index from Korean sentence
    :return: splited
    """
    if ' ' in kr_pron:
        out = list()
        i = 0
        split = kr_pron.split(' ')
        for k in split:
            len_k = len(k)
            if len_k > 1:
                out.append((entity[i:i+len_k], k, idx+i))
            i += len_k
        return out
    else:
        return [(entity, kr_pron, idx)]


def extract_entities_from_dataset(kr: str):
    """
    Extract named entities which are parenthesized() with Korean pronounce from Korean sentences.
    :param kr: Korean sentences from text files including word written by Korean and Chinese together.
    :return: entities with Korean pronounce
    """
    # step 1. Find entities in Korean sentences
    # which Chinese words composed of two or more Chinese character
    entities_cand = [(m.group(), m.start()) for m in ne_from_kr.finditer(kr)]
    # find Chinese words are parenthesized[] to remove these
    not_entities = [(m.group().strip('[]'), m.start()+1) for m in not_ne.finditer(kr)]

    # remove Chinese words which are parenthesized[]
    entities_with_idx = []
    for cand in entities_cand:
        if cand not in not_entities:
            entities_with_idx.append(cand)
    # 문신(文臣)의 제술(製述)은 장례원 사의(掌隷院司議) 표연말(表沿沫)이 으뜸을 차지하니, 명하여 한 자급을 더하게 하였다.
    # entity: (文臣), start_index: 2
    # [('文臣', 2), ('製述', 10), ('掌隷院司議', 22), ('表沿沫', 33)]

    # step 2. get entity pronounce from Korean sentence e.g. 문신(文臣)
    # [('春塘臺', '춘당대', 4), ('隨駕', '수가', 17), ('軍校', '군교', 25), ('試射', '시사', 34)]
    entities_with_kr = list(map(lambda x: pronounce_from_kr(kr, x[0], x[1]), entities_with_idx))

    # step 3. remove digit entity e.g. ('四更', '4경') will be removed
    entities_with_kr = rm_digit_entity(entities_with_kr)

    # step4. split 2 words in Korean but 1 words in Chinese
    # e.g. 포도 대장(捕盜大將) -> 포도(捕盜),  대장(大將)
    entities = list()
    for e, k, i in entities_with_kr:
        split = entity_split_by_space(e, k, i)
        entities.extend(split)

    return entities


def preprocess_korean_from_sentence(kr_sent: str, tagger, reg_dic: Dict):
    # step 1. get Korean sentence from a text file and remove redundant line
    kr = kr_sent.strip()

    # step 2. extract Chinese entities from Korean sentence
    # [('春塘臺', '춘당대', 4), ...]
    entities = extract_entities_from_dataset(kr)

    # step 3. Chinese character regularize
    entities = list(map(lambda x: (chinese_regularize(x[0], reg_dic), x[1], x[2]), entities))

    # step 4. clean up Korean sentence i.e. remove (製述) and 031)
    kr = rm_redundant_from_kr.sub('', kr)

    # step 5. tokenize using updated mecab tagger (updated by SILOK entity dictionary)
    # TODO remove ('SY', '기타 기호') tokens
    morp = tagger.morphs(kr)
    # morp = tagger.pos(kr)

    # step 6. return Korean sentence & tokenized & entities
    return kr, morp, list(map(lambda x: (x[0], x[1]), entities))


def my_test():
    reg_dic = read_dic('../Dictionary/compatibility2unified.txt', start_line=2)
    tagger = Mecab()

    # import random
    # kr_sents = random.sample(read_xlsx('xlsx/train8_test2/train.xlsx', [1]), 30)
    kr_sents = read_xlsx('Languagemodel dataset.xlsx', [3])

    for sent in kr_sents:
        kr, tokens, entities = preprocess_korean_from_sentence(sent, tagger, reg_dic)
        print('===========================================================================')
        print(sent)
        print(kr)
        print(tokens)
        print(entities)
        print('===========================================================================')


if __name__ == '__main__':
    my_test()
