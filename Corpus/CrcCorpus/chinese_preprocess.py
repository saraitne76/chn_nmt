import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(os.path.dirname(__file__)))))

from Corpus.CrcCorpus.korean_preprocess import *           # noqa
from typing import Optional                                     # noqa

not_ch = re.compile(r'[^\u2E80-\u2EFF\u3400-\u4DBF\u4E00-\u9FBF\uF900-\uFAFF]')
space = re.compile(r'\s{2,}')


def get_entity(line: str,
               ne_list: List[str],
               major_ne_list: Optional[List] = None) -> List[Tuple[str, int]]:
    """
    Find entity in Chinese sentence based on lexicon
    """
    entities = []
    temp = line

    if major_ne_list is not None:
        for ne in major_ne_list:
            flag = False
            s = 0
            while True:
                ne_idx = temp.find(ne, s)
                if ne_idx < 0:
                    break
                else:
                    flag = True
                    s = ne_idx + len(ne)
                    entities.append((ne, ne_idx))
            if flag:
                temp = temp.replace(ne, '-' * (len(ne)))

    for ne in ne_list:
        flag = False
        s = 0
        while True:
            ne_idx = temp.find(ne, s)
            if ne_idx < 0:
                break
            else:
                flag = True
                s = ne_idx + len(ne)
                entities.append((ne, ne_idx))
        if flag:
            temp = temp.replace(ne, '-'*(len(ne)))
    return entities


def tokenize_ch(ch: str, entities: List[Tuple[str, int]]):
    if len(entities) <= 0:
        return list(ch)
    else:
        entities.sort(key=lambda item: item[1])
        temp = []
        idx = 0
        entity_cnt = 0
        while True:
            if idx == entities[entity_cnt][1]:
                temp.append(entities[entity_cnt][0])
                idx += len(entities[entity_cnt][0])
                entity_cnt += 1
                if entity_cnt >= len(entities):
                    entity_cnt = len(entities) - 1
            elif ch[idx] == ' ':
                idx += 1
            else:
                temp.append(ch[idx])
                idx += 1
            if idx >= len(ch):
                break
        return temp


def clean_up_ch(ch: str, remove_mark=True):
    """
    Remove all character which is not Chinese.
    if remove_mark = True, all punctuation marks are replaced into ''
    if remove_mark = False, all punctuation marks are preserved as single space ' '
    """
    if remove_mark:
        return not_ch.sub('', ch)
    else:
        return space.sub(' ', not_ch.sub(' ', ch))


def preprocess_chinese_from_sentence(pair: Tuple[str, str], lexicon: List[str], reg_dic: Dict, rm_mark: bool):
    """
    :param pair: Chinese - Korean pair
    :param lexicon: entity list
    :param reg_dic: regularization dictionary
    :param rm_mark: if True, all mark are removed,
                    if False, all mark are converted into single space,
                                    and token split according to space and entities
    :return: cleaned chinese sentence, chinese tokens, entities
    """
    # step 0. get entity list from korean sentence
    kr = pair[1].strip()

    entities_cand = [m.group() for m in ne_from_kr.finditer(kr)]
    # find Chinese words are parenthesized[] to remove these
    not_entities = [(m.group().strip('[]'), m.start()+1) for m in not_ne.finditer(kr)]
    # remove Chinese words which are parenthesized[]
    entities_from_kr = []
    for cand in entities_cand:
        if cand not in not_entities:
            entities_from_kr.append(cand)

    entities_from_kr = list(map(lambda x: chinese_regularize(x, reg_dic), entities_from_kr))

    # step 1. get Chinese sentence from a text file and remove redundant line
    ch = pair[0].strip()

    # step 2. clean up and regularize chinese sentence
    ch = clean_up_ch(chinese_regularize(ch, reg_dic), remove_mark=rm_mark)

    # step 4. search entity from chinese sentence based on lexicon
    entities = get_entity(ch, lexicon, entities_from_kr)
    entities.sort(key=lambda x: x[1])

    # step 5. tokenize Chinese sentence based on entity list
    tokens = tokenize_ch(ch, entities)

    return ch, tokens, entities


def my_test():
    reg_dic = read_dic('../Dictionary/compatibility2unified.txt', start_line=2)
    lexicon = list(read_dic('lexicon.txt', start_line=0).keys())
    lexicon.sort(key=lambda x: len(x), reverse=True)

    ch_kr_pairs = read_xlsx('Languagemodel dataset.xlsx', [2, 3])

    for pair in ch_kr_pairs:
        ch, tokens1, entities1 = preprocess_chinese_from_sentence(pair, lexicon, reg_dic, rm_mark=True)
        ch, tokens2, entities2 = preprocess_chinese_from_sentence(pair, lexicon, reg_dic, rm_mark=False)
        print('===========================================================================')
        print(ch)
        print(tokens1)
        print(entities1)
        print(tokens2)
        print(entities2)
        print('===========================================================================')


if __name__ == '__main__':
    my_test()
