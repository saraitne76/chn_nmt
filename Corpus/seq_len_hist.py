import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

from utils import *                             # noqa
import matplotlib                               # noqa
import matplotlib.pyplot as plt                 # noqa
import argparse                                 # noqa


def seq_hist(data_path: str, bins: int):

    matplotlib.rcParams.update({'font.size': 17})

    data = list()
    for d in data_path:
        if os.path.isdir(d):
            data.extend(read_pickles(d))
        else:
            data.extend(pickle_load(d))
    # if os.path.isdir(data_path):
    #     data = read_pickles(data_path)
    # else:
    #     data = pickle_load(data_path)
    print('number of data: %d' % len(data))

    # get whole train data sequence length
    len_ch_tok = list(map(lambda x: x.ch_seq_len, data))
    len_kr_tok = list(map(lambda x: x.kr_seq_len, data))

    # sort (big value is latter)
    len_ch_tok.sort()
    len_kr_tok.sort()

    # drop biggest 5%
    len_ch_tok = len_ch_tok[:int(0.95 * len(len_ch_tok))]
    len_kr_tok = len_kr_tok[:int(0.95 * len(len_kr_tok))]

    fig = plt.figure(1)
    ax1 = fig.add_subplot(2, 1, 1)
    # plt.xlabel('Sequence Length')
    plt.ylabel('# Data')

    ax2 = fig.add_subplot(2, 1, 2)
    plt.xlabel('Sequence Length')
    plt.ylabel('# Data')

    ax1.hist(len_kr_tok, bins, label='Korean', color='blue', edgecolor='black')
    ax1.legend(loc='upper right')
    ax2.hist(len_ch_tok, bins, label='Chinese', color='red', edgecolor='black')
    ax2.legend(loc='upper right')
    # plt.hist(len_kr_tok, bins, alpha=0.5, label='KoreanToken', color='blue')
    # plt.hist(len_ch_tok, bins, alpha=0.5, label='ChineseToken', color='red')
    # plt.legend(loc='upper right')

    ax1.set_xlim([0, 800])
    ax1.set_ylim([0, 125000])

    ax2.set_xlim([0, 800])
    ax2.set_ylim([0, 125000])

    print('CHI mean', np.mean(len_ch_tok))
    print('CHI median', np.median(len_ch_tok))

    print('KOR mean', np.mean(len_kr_tok))
    print('KOR median', np.median(len_kr_tok))

    plt.show()


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--data', type=str, default='split_X/train', nargs='+',
                        help='Pair data path')
    parser.add_argument('-b', '--bins', type=int, default=10,
                        help='The number of histogram bins')
    args = parser.parse_args()

    seq_hist(args.data, args.bins)


if __name__ == '__main__':
    main()
