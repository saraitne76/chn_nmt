import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

from utils import *                             # noqa
import argparse                                 # noqa


def data_count(data_path: str, ch_max: int, kr_max: int, log: str):
    if os.path.isdir(data_path):
        data = read_pickles(data_path)
    else:
        data = pickle_load(data_path)

    total = len(data)

    cnt = 0
    for d in data:
        if d.ch_seq_len <= ch_max and d.kr_seq_len <= kr_max:
            cnt += 1
    print_write("""
    ======================================
    Data path: %s
    Chinese Token Max Length: %d 
    Korean Token Max Length: %d
    The Number of Data
    Remain: %d (%0.2f%%)
    Drop: %d
    Total: %d
    ======================================
    \r""" % (data_path, ch_max, kr_max, cnt, (cnt/total)*100, total-cnt, total), log, mode='a')


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--data', type=str, default='split_X/train',
                        help='Pair data path')
    parser.add_argument('-c', '--ch_max', type=int, default=100,
                        help='Chinese sequence max length')
    parser.add_argument('-k', '--kr_max', type=int, default=200,
                        help='Korean sequence max length')
    parser.add_argument('-l', '--log_file', type=str, default='data_clip_cnt.txt',
                        help='Log text file name')
    args = parser.parse_args()

    data_count(args.data, args.ch_max, args.kr_max, args.log_file)


if __name__ == '__main__':
    main()
