import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

from utils import *                                         # noqa
import random                                               # noqa

lexicon_crc = read_lexicon('CrcCorpus/lexicon_crc.txt')
lexicon_silok = read_lexicon('SilokCorpus/lexicon_silok.txt')
lexicon = read_lexicon('lexicon.txt')

print(random.sample(lexicon_crc, 10))
print(len(lexicon_crc))
print(random.sample(lexicon_silok, 10))
print(len(lexicon_silok))
print(random.sample(lexicon, 10))
print(len(lexicon))
