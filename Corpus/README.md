# Korean tokenization (morpheme analysis) 

## Install
### 1.1. Konlpy
#### Install [**KoNLPy**](https://konlpy-ko.readthedocs.io/ko/v0.4.3/install/#ubuntu) python package.
```  
 $ sudo apt-get install g++ openjdk-7-jdk    # Install Java 1.7+
 $ sudo apt-get install python3-dev          # Python 3.x
 $ pip3 install konlpy                       # Python 3.x 
```

### 1.2. [**Mecab**](https://bitbucket.org/eunjeon/mecab-ko-dic/src/master/)
#### Download [**mecab-ko**](https://bitbucket.org/eunjeon/mecab-ko/downloads/) source code.  
```
 $ tar xvzf mecab-XX-ko-XX.tar.gz       # unzip source code zip file
 $ cd mecab-XX-ko-XX                    # go to source code directory
``` 

#### Compile source code.
```
 $ ./configure
 $ make
 $ make check
 $ su
 $ make install
```

#### Download [**mecab-ko-dic**](https://bitbucket.org/eunjeon/mecab-ko/downloads/) source code.  
```
 $ tar xvzf mecab-ko-dic-XX.tar.gz      # unzip source code zip file
 $ cd mecab-ko-dic-XX                   # go to source code directory
``` 

#### Compile source code.
```
 $ ./configure
 $ make
 $ su
 $ make install
```
> automake 버전 문제로 설치 도중 에러가 나는 경우, 다음과 같이 할 수 있습니다.  
> automake1.11 설치 후, 위와 동일하게 재시도 혹은,  
> autogen.sh 실행 후 재시도  
> ```
> $ tar zxfv mecab-ko-dic-XX.tar.gz  
> $ cd mecab-ko-dic-XX  
> $ ./autogen.sh  
> $ configure  
> $ make  
> $ su
> $ make install
> ```  

> libmecab.so.2를 찾을 수 없는 에러가 나는 경우, 다음과 같이 할 수 있습니다.  
> error while loading shared libraries: libmecab.so.2: cannot open shared object file: No such file or directory  
> 라이브러리를 다시 링크하고 확인후 재시도  
> ```
> $ sudo ldconfig
> ```

#### Mecab-python
```
 $ git clone https://bitbucket.org/eunjeon/mecab-python-0.996.git
 $ cd mecab-python-0.996
 $ python setup.py build
 $ python setup.py install
```

#### [Edit Dictionary](https://bitbucket.org/eunjeon/mecab-ko-dic/src/df15a487444d88565ea18f8250330276497cc9b9/final/user-dic/)
`$ cd mecab-kodic-XX/user-dic` and check dictionary file format (.csv)  Add your own dictionary file
```
userdic/
├── nnp.csv
├── person.csv
├── place.csv
├── my_dic1.csv
└── my_dic2.csv
```  
```
 $ mecab-ko-dic-XX/tools/add-userdic.sh
```
아래와 같이 user-xxx.csv 사전이 추가된 모습을 볼 수 있습니다. 사실 아래 파일은 컴파일 되기 직전의 파일이며,  
실제로 `/usr/local/lib/mecab/dic/mecab-ko-dic/sys.dic` 파일에 바이너리로 컴파일 되어 들어가게 됩니다.
```
mecab-ko-dic
├── ....
├── user-nnp.csv
├── user-person.csv
├── user-place.csv
├── user-my_dic1.csv
├── user-my_dic2.csv
└── ...
```
Install user dictionary 
```
 $ cd mecab-ko-dic-XX
 $ make install
```

<!-- ## How to use -->
