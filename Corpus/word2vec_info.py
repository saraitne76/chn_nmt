import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))

from utils import *                 # noqa
import argparse                     # noqa


def main(folder):
    files = os.listdir(folder)
    models = list()
    for file in files:
        models.append(file.split('.')[0])
    models = list(set(models))
    models.sort()
    f = open(os.path.join(folder, CONFIG_TXT), 'w')
    for model in models:
        m = os.path.join(folder, str(model) + '.embed')
        # print(m)
        try:
            word2id, embed_vectors, embed_dim = load_word2vec(m, [PAD, UNK])
            f.write('=================================================================================\n')
            f.write('Model: %s\n' % m)
            f.write('Dimension: %s\n' % embed_dim)
            f.write('Vocabulary Size: %s\n' % len(word2id))
            f.write('%s\n' % random.sample(word2id.items(), 10))
            f.write('%s\n' % random.sample(word2id.items(), 10))
            f.write('%s\n' % random.sample(word2id.items(), 10))
            f.write('=================================================================================\n')
        except FileNotFoundError:
            pass


if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-d', '--dir', type=str,
                        help='word2vec folder')
    args = parser.parse_args()

    main(args.dir)
